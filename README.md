# Description

Provides the codes used for analyzing imaging data, clinical electroanatomic mapping data from catheter ablation procedures, and simulation data generated using the Cardiac Arrhythmia Research Package (CARP) software for Sung et. al 2022 _Nature Cardiovascular Research_. The open source software openCARP (https://opencarp.org) can be used to generate the same simulation files.

## Getting started

All codes require MATLAB to be installed. Codes were written with respect to MATLAB version 2020A or newer. For MATLAB installation, follow the instructions at https://www.mathworks.com. In addition, the "Statistics & Machine Learning Toolbox" should be added upon installation. All software below should run on any standard computer with no specific, special memory requirements. 

## Data files
Original mesh files and simulations files are available upon reasonable request. Raw clinical electroanatomic mapping data and imaging data cannot be readily shared due to patient privacy concerns. Specific requests for access to this data may be made in consultation with our institution's review board.

Data files provided here (in the form of MATLAB .mat files) represent summary measurements obtained through processing of imaging, electroanatomic mapping data, and simulation data. Analysis codes provided below use these .mat files as inputs to generate the data and figures presented in the Results section. 

## Helper functions
violin.m = code for generating violin plots

linspecer.m = color coding for plots

## (1) Analyzing fat and scar distributions
**INFINITY_compareFatandScar_byAHA_onlyAnalysis.m** = Assesses the inFAT and scar across anatomical regions. It also plots the amount of inFAT versus the amount of scar. 

	Inputs: INFINITY_substrateInfo.mat, Output: Plots comparing inFAT and scar distributions, used to create Figure 2 in the manuscript

**INFINITY_compareFatandScar_intramural_byUVC_onlyAnalysis.m** = Assesses the inFAT and scar intramurally. This figure is in the Supplementary Materials. It does NOT look at the segmentations, it looks at the meshes which must have the UVC coordinates already computed for them 

	Inputs: INFINITY_substrate_transmural_UVC_2nd.mat, Output: histograms comparing inFAT and scar distributions intramurally, processed to create Supplemental Figure 8

## (2) Analyzing electroanatomic mapping data
**INFINITY_EAMdata_onlyAnalysis.m** = Analyzes the electroanatomic mapping data and compares voltage + deceleration zones with inFAT and scar distributions. This corresponds to Figure 3 in the manuscript 

	Inputs: INFINITY_voltageData_EAM.mat, INFINITY_all_abnormalEAMData.mat, Outputs: Plots comparing voltage and deceleration zones in scar and fat distributions

## (3) Analyzing VT circuit simulation data
**INFINITY_VTcircuit_analysis_for_CT_and_MRI.m** = Analyzes the VT circuits in CT- and MRI-based only models. Computes statistics about conduction velocity and inFAT/scar distributions in the circuits. Assesses the distribution of VTs across models

	Inputs: analyze_VTannotations.mat, extra_VTannotation_variables.mat, INFINITY_CVstats_throughoutVTcircuit.mat,INFINITY_tissueVolumes_throughoutVTcircuit.mat, INFINITY_VTcircuit_dynamics.mat

**INFINITY_VTcircuit_analysis_for_hybridCTMRI.m** = Analyzes the VT circuits in hybrid CT-MRI models. Computes statistics about conduction velocity and inFAT/scar distributions in the circuits. Assesses the distribution of VTs across hybrid CT-MRI models

	Inputs: analyze_VTannotations_CTMRI.mat, extra_VTannotation_variables_hybridCTMRI.mat, INFINITY_CVstats_throughoutVTcircuit_CTMRI.mat,INFINITY_tissueVolumes_throughoutVTcircuit_CTMRI.mat, INFINITY_VTcircuit_dynamics_CTMRI.mat

## (4) Comparing both simulation and clinical electroanatomic mapping data
**INFINITY_analyze_EAMData_wRemodelingandVTcircuits.m** = Compares VT circuits with co-registered EAM data to determine the mechanistic contributions of inFAT and/or scar to pro-arrhythmogenic properties

	Inputs: ABLandDZ_withinVTcircuit_CTMRI.mat, INFINITY_scarandfat_VTcircuit.mat, INFINITY_scarandfat_VTcircuit.mat, ablation_scarandfat_VTcircuit.mat, ablation_scarandfat_VTcircuit_CTMRI.mat

	Outputs: Statistics relating the amount of inFAT and/or scar to the amount of ablated tissues and deceleration zones in a given VT circuit.

