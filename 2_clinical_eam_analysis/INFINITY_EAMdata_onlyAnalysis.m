% INFINITY_EAMdata_onlyAnalysis.m
% Performs analysis on EAM data focusing on voltage and deceleration zone
% data

%% Step 1 Perform the analysis of voltage/dz data with inFAT/scar
% This section extracts the substrate density from the different patients

load('INFINITY_voltageData_EAM.mat')
load('INFINITY_all_abnormalEAMData.mat') % all_abnormalEAMData

% pt336 2 and 3 are epicardial EAMs and should be compared with the
% epicardium, not endocardium

% Intializing variables

allPts_normalCT = [];
allPts_inFAT = [];
allPts_admixture = [];

allPts_normalCT_10mm = [];
allPts_inFAT_10mm = [];
allPts_admixture_10mm = [];

allPts_normalCT_15mm = [];
allPts_inFAT_15mm = [];
allPts_admixture_15mm = [];

allPts_normalMRI = [];
allPts_scar = [];
allPts_greyZone = [];

allPts_normalMRI_10mm = [];
allPts_scar_10mm = [];
allPts_greyZone_10mm = [];

allPts_normalMRI_15mm = [];
allPts_scar_15mm = [];
allPts_greyZone_15mm = [];

allPts_MRI_D = [];
allPts_CT_D = [];

% EAM variables
allPts_unipolarCT = [];
allPts_bipolarCT = [];
allPts_unipolarMRI = [];
allPts_bipolarMRI = [];
allPts_dzCT = []; % Deceleration zones
allPts_dzMRI = []; % Deceleration zones

% Individual max - min LATs (for reviewer question)
% Just do it for CT since you just need the EAMs, no need to repeat
LATs_by_EAM = [];

corr_rho_byMap_bipolar = [];
corr_p_byMap_bipolar = [];

fnames_vData = fieldnames(voltageData); % fieldnames for voltage Data
EAM_map_ID = [];
allPts_tagsCT = [];
allPts_tagsMRI = [];
CT_EAMcount = [0]; % Count the number of points per EAM for CT comparisons
MRI_EAMcount = [0]; % Count the number of points per EAM for MRI comparisons
CT_EAMnames = {};
MRI_EAMnames = {};
for p = 1:length(fnames_vData)
    base_ptname = fnames_vData{p}(1:5); % for all_abnormalEAMData
    yesCT = mod(p,2)==1; % The way the data is structured is that odd integers are CT, even integers are MRI
    yesMRI = mod(p,2)==0;
    EAM_names = fieldnames(voltageData.(fnames_vData{p})); % Names for electroanatomic names
    %if length(voltageData{p})>1
        for d = 1:length(EAM_names)
           noVolt = (abs(voltageData.(fnames_vData{p}).(EAM_names{d}).unipolar)>=1e4)|...
                (abs(voltageData.(fnames_vData{p}).(EAM_names{d}).bipolar)>=1e4);
           abnormalEAM_d = all_abnormalEAMData.(base_ptname).(EAM_names{d});
           num_EAMpts = voltageData.(fnames_vData{p}).(EAM_names{d}).num_points;     
           tmp_tags = zeros(num_EAMpts,1);
           tmp_tags(all_abnormalEAMData.(base_ptname).(EAM_names{d}).meshIDs) = ...
               all_abnormalEAMData.(base_ptname).(EAM_names{d}).tags; 
           tmp_tags(noVolt) = [];
           if yesCT % if CT
               lCT = length(allPts_CT_D);
               allPts_tagsCT = [allPts_tagsCT; tmp_tags];
               if (strcmp(fnames_vData{p},'pt336_CT')||strcmp(fnames_vData{p},'pt336_MRI'))&&... % If epi then compare with Depi instead
                   (strcmp(EAM_names{d},'EAM2')||strcmp(EAM_names{d},'EAM3'))
                   allPts_normalCT = [allPts_normalCT; voltageData.(fnames_vData{p}).(EAM_names{d}).epi_normal(~noVolt)]; % Go through, find only the points with voltages
                   allPts_inFAT = [allPts_inFAT; voltageData.(fnames_vData{p}).(EAM_names{d}).epi_dense(~noVolt)]; % And record the number of points of normal, dense, and mixed myocardium
                   allPts_admixture = [allPts_admixture; voltageData.(fnames_vData{p}).(EAM_names{d}).epi_mixed(~noVolt)]; % Records the amount of each tissue type
                   
                   allPts_normalCT_10mm = [allPts_normalCT_10mm; voltageData.(fnames_vData{p}).(EAM_names{d}).epi_normal_10mm(~noVolt)]; % Do this for 10 mm
                   allPts_inFAT_10mm = [allPts_inFAT_10mm; voltageData.(fnames_vData{p}).(EAM_names{d}).epi_dense_10mm(~noVolt)];
                   allPts_admixture_10mm = [allPts_admixture_10mm; voltageData.(fnames_vData{p}).(EAM_names{d}).epi_mixed_10mm(~noVolt)];
                   
                   allPts_normalCT_15mm = [allPts_normalCT_15mm; voltageData.(fnames_vData{p}).(EAM_names{d}).epi_normal_15mm(~noVolt)]; % And again for 15 mm
                   allPts_inFAT_15mm = [allPts_inFAT_15mm; voltageData.(fnames_vData{p}).(EAM_names{d}).epi_dense_15mm(~noVolt)];
                   allPts_admixture_15mm = [allPts_admixture_15mm; voltageData.(fnames_vData{p}).(EAM_names{d}).epi_mixed_15mm(~noVolt)];
                   
                   %allPts_substrateCT = [allPts_substrateCT; voltageData{p}(d).allSubstrate(~noVolt)];
                   allPts_unipolarCT = [allPts_unipolarCT; voltageData.(fnames_vData{p}).(EAM_names{d}).unipolar(~noVolt)]; % Also pull out the correspond voltages
                   allPts_bipolarCT= [allPts_bipolarCT; voltageData.(fnames_vData{p}).(EAM_names{d}).bipolar(~noVolt)]; % bipolar voltage
                   allPts_dzCT= [allPts_dzCT; voltageData.(fnames_vData{p}).(EAM_names{d}).DZ(~noVolt)]; % deceleration zones
                   allPts_CT_D = [allPts_CT_D; voltageData.(fnames_vData{p}).(EAM_names{d}).registration_Depi(~noVolt)]; % Pull out the registration distance to the digital heart surface
                   CT_EAMcount = [CT_EAMcount; (length(allPts_CT_D)-lCT)];
                   CT_EAMnames{end+1} = [fnames_vData{p} '_' EAM_names{d}];
                   
                   % Get max-min LAT
                   LATs_by_EAM = [LATs_by_EAM;...
                            max(voltageData.(fnames_vData{p}).(EAM_names{d}).lats(~noVolt))-...
                            min(voltageData.(fnames_vData{p}).(EAM_names{d}).lats(~noVolt))];
                   
               else % These are the cases where mapping was performed only endocardially
                   allPts_normalCT = [allPts_normalCT; voltageData.(fnames_vData{p}).(EAM_names{d}).endo_normal(~noVolt)];
                   allPts_inFAT = [allPts_inFAT; voltageData.(fnames_vData{p}).(EAM_names{d}).endo_dense(~noVolt)];
                   allPts_admixture = [allPts_admixture; voltageData.(fnames_vData{p}).(EAM_names{d}).endo_mixed(~noVolt)];
                   
                   allPts_normalCT_10mm = [allPts_normalCT_10mm; voltageData.(fnames_vData{p}).(EAM_names{d}).endo_normal_10mm(~noVolt)];
                   allPts_inFAT_10mm = [allPts_inFAT_10mm; voltageData.(fnames_vData{p}).(EAM_names{d}).endo_dense_10mm(~noVolt)];
                   allPts_admixture_10mm = [allPts_admixture_10mm; voltageData.(fnames_vData{p}).(EAM_names{d}).endo_mixed_10mm(~noVolt)];
                   
                   allPts_normalCT_15mm = [allPts_normalCT_15mm; voltageData.(fnames_vData{p}).(EAM_names{d}).endo_normal_15mm(~noVolt)];
                   allPts_inFAT_15mm = [allPts_inFAT_15mm; voltageData.(fnames_vData{p}).(EAM_names{d}).endo_dense_15mm(~noVolt)];
                   allPts_admixture_15mm = [allPts_admixture_15mm; voltageData.(fnames_vData{p}).(EAM_names{d}).endo_mixed_15mm(~noVolt)];
                   
                   %allPts_substrateCT = [allPts_substrateCT; voltageData{p}(d).allSubstrate(~noVolt)];
                   allPts_unipolarCT = [allPts_unipolarCT; voltageData.(fnames_vData{p}).(EAM_names{d}).unipolar(~noVolt)];
                   allPts_bipolarCT= [allPts_bipolarCT; voltageData.(fnames_vData{p}).(EAM_names{d}).bipolar(~noVolt)];
                   allPts_dzCT= [allPts_dzCT; voltageData.(fnames_vData{p}).(EAM_names{d}).DZ(~noVolt)]; % deceleration zones
                   allPts_CT_D = [allPts_CT_D; voltageData.(fnames_vData{p}).(EAM_names{d}).registration_Dendo(~noVolt)];
                   CT_EAMcount = [CT_EAMcount; (length(allPts_CT_D)-lCT)];
                   CT_EAMnames{end+1} = [fnames_vData{p} '_' EAM_names{d}];
                   LATs_by_EAM = [LATs_by_EAM;...
                            max(voltageData.(fnames_vData{p}).(EAM_names{d}).lats(~noVolt))-...
                            min(voltageData.(fnames_vData{p}).(EAM_names{d}).lats(~noVolt))];
                   
               end
           elseif yesMRI % if MRI
               lMRI = length(allPts_MRI_D); % Previous length of vector
               allPts_tagsMRI = [allPts_tagsMRI; tmp_tags];
               if (strcmp(fnames_vData{p},'pt336_CT')||strcmp(fnames_vData{p},'pt336_MRI'))&&... % If epi then compare with Depi instead
                   (strcmp(EAM_names{d},'EAM2')||strcmp(EAM_names{d},'EAM3'))
                   allPts_normalMRI = [allPts_normalMRI; voltageData.(fnames_vData{p}).(EAM_names{d}).epi_normal(~noVolt)];
                   allPts_scar = [allPts_scar; voltageData.(fnames_vData{p}).(EAM_names{d}).epi_dense(~noVolt)];
                   allPts_greyZone = [allPts_greyZone; voltageData.(fnames_vData{p}).(EAM_names{d}).epi_mixed(~noVolt)];
                   
                   allPts_normalMRI_10mm = [allPts_normalMRI_10mm; voltageData.(fnames_vData{p}).(EAM_names{d}).epi_normal_10mm(~noVolt)];
                   allPts_scar_10mm = [allPts_scar_10mm; voltageData.(fnames_vData{p}).(EAM_names{d}).epi_dense_10mm(~noVolt)];
                   allPts_greyZone_10mm = [allPts_greyZone_10mm; voltageData.(fnames_vData{p}).(EAM_names{d}).epi_mixed_10mm(~noVolt)];
                   
                   allPts_normalMRI_15mm = [allPts_normalMRI_15mm; voltageData.(fnames_vData{p}).(EAM_names{d}).epi_normal_15mm(~noVolt)];
                   allPts_scar_15mm = [allPts_scar_15mm; voltageData.(fnames_vData{p}).(EAM_names{d}).epi_dense_15mm(~noVolt)];
                   allPts_greyZone_15mm = [allPts_greyZone_15mm; voltageData.(fnames_vData{p}).(EAM_names{d}).epi_mixed_15mm(~noVolt)];
                   
                   %allPts_substrateCT = [allPts_substrateCT; voltageData{p}(d).allSubstrate(~noVolt)];
                   allPts_unipolarMRI = [allPts_unipolarMRI; voltageData.(fnames_vData{p}).(EAM_names{d}).unipolar(~noVolt)];
                   allPts_bipolarMRI= [allPts_bipolarMRI; voltageData.(fnames_vData{p}).(EAM_names{d}).bipolar(~noVolt)];
                   allPts_dzMRI= [allPts_dzMRI; voltageData.(fnames_vData{p}).(EAM_names{d}).DZ(~noVolt)]; % deceleration zones
                   allPts_MRI_D = [allPts_MRI_D; voltageData.(fnames_vData{p}).(EAM_names{d}).registration_Depi(~noVolt)];
                   MRI_EAMcount = [MRI_EAMcount; (length(allPts_MRI_D)-lMRI)];
                   MRI_EAMnames{end+1} = [fnames_vData{p} '_' EAM_names{d}];
               else
                   allPts_normalMRI = [allPts_normalMRI; voltageData.(fnames_vData{p}).(EAM_names{d}).endo_normal(~noVolt)];
                   allPts_scar = [allPts_scar; voltageData.(fnames_vData{p}).(EAM_names{d}).endo_dense(~noVolt)];
                   allPts_greyZone = [allPts_greyZone; voltageData.(fnames_vData{p}).(EAM_names{d}).endo_mixed(~noVolt)];
                   
                   allPts_normalMRI_10mm = [allPts_normalMRI_10mm; voltageData.(fnames_vData{p}).(EAM_names{d}).endo_normal_10mm(~noVolt)];
                   allPts_scar_10mm = [allPts_scar_10mm; voltageData.(fnames_vData{p}).(EAM_names{d}).endo_dense_10mm(~noVolt)];
                   allPts_greyZone_10mm = [allPts_greyZone_10mm; voltageData.(fnames_vData{p}).(EAM_names{d}).endo_mixed_10mm(~noVolt)];
                   
                   allPts_normalMRI_15mm = [allPts_normalMRI_15mm; voltageData.(fnames_vData{p}).(EAM_names{d}).endo_normal_15mm(~noVolt)];
                   allPts_scar_15mm = [allPts_scar_15mm; voltageData.(fnames_vData{p}).(EAM_names{d}).endo_dense_15mm(~noVolt)];
                   allPts_greyZone_15mm = [allPts_greyZone_15mm; voltageData.(fnames_vData{p}).(EAM_names{d}).endo_mixed_15mm(~noVolt)];
                   
                   %allPts_substrateCT = [allPts_substrateCT; voltageData{p}(d).allSubstrate(~noVolt)];
                   allPts_unipolarMRI = [allPts_unipolarMRI; voltageData.(fnames_vData{p}).(EAM_names{d}).unipolar(~noVolt)];
                   allPts_bipolarMRI= [allPts_bipolarMRI; voltageData.(fnames_vData{p}).(EAM_names{d}).bipolar(~noVolt)];
                   allPts_dzMRI= [allPts_dzMRI; voltageData.(fnames_vData{p}).(EAM_names{d}).DZ(~noVolt)]; % deceleration zones
                   allPts_MRI_D = [allPts_MRI_D; voltageData.(fnames_vData{p}).(EAM_names{d}).registration_Dendo(~noVolt)];
                   MRI_EAMcount = [MRI_EAMcount; (length(allPts_MRI_D)-lMRI)];
                   MRI_EAMnames{end+1} = [fnames_vData{p} '_' EAM_names{d}];
               end
           end
           

       end

end
CT_EAMcount(1) = []; MRI_EAMcount(1) = []; % Remove the 0 that was added
EAM_CT_index = []; % ID tag for each EAM in each patient
EAM_MRI_index = [];

% This procedure allows us to aggregate all the data into a single vector
% but simultaneously keep track of which data file each entry belongs to.
tmpCT_s = [1; cumsum(CT_EAMcount(1:end-1))+1];
tmpMRI_s = [1; cumsum(MRI_EAMcount(1:end-1))+1];
tmpCT_e = cumsum(CT_EAMcount);
tmpMRI_e = cumsum(MRI_EAMcount);
for u = 1:length(CT_EAMcount)
   EAM_CT_index(tmpCT_s(u):tmpCT_e(u)) = u;
   EAM_MRI_index(tmpMRI_s(u):tmpMRI_e(u)) = u;
end

% Process LATs range
LATs_by_EAM(LATs_by_EAM==0) = [];

%% Section 2 Looking at correlation analyses

min_distance_allowed = 5; % minimum distance allowed
CT_close = allPts_CT_D<=min_distance_allowed;
MRI_close = allPts_MRI_D<=min_distance_allowed;
both_CTMRI_close = CT_close&MRI_close; % Only consider points that are close enough after registration

paceMap = allPts_tagsCT==15|allPts_tagsCT==16;
LAVA = allPts_tagsCT==7|allPts_tagsCT==8|allPts_tagsCT==24|allPts_tagsCT==26; % Tags corresponding to LAVAs
paceMap_bothCTMRI = paceMap(both_CTMRI_close);
LAVA_bothCTMRI = LAVA(both_CTMRI_close);

% Variables for both CT and MRI comparison
% allPts here refers to number of points in a given radius of consideration
% start from 5 mm and go all the way to 15 mm
% bothCTMRI just means we're considering both fat and scar in the same
% given space
EAM_CT_index_bothCTMRI = EAM_CT_index(both_CTMRI_close)';
EAM_MRI_index_bothCTMRI = EAM_MRI_index(both_CTMRI_close)';

allPts_scar_bothCTMRI = allPts_scar(both_CTMRI_close); 
allPts_greyZone_bothCTMRI = allPts_greyZone(both_CTMRI_close);
allPts_substrateMRI_bothCTMRI = allPts_scar_bothCTMRI + allPts_greyZone_bothCTMRI;
allPts_normalMRI_bothCTMRI = allPts_normalMRI(both_CTMRI_close);
allPts_inFAT_bothCTMRI = allPts_inFAT(both_CTMRI_close);
allPts_admixture_bothCTMRI = allPts_admixture(both_CTMRI_close);
allPts_substrateCT_bothCTMRI = allPts_inFAT_bothCTMRI + allPts_admixture_bothCTMRI;
allPts_normalCT_bothCTMRI = allPts_normalCT(both_CTMRI_close);

allPts_scar_10mm_bothCTMRI = allPts_scar_10mm(both_CTMRI_close);
allPts_greyZone_10mm_bothCTMRI = allPts_greyZone_10mm(both_CTMRI_close);
allPts_substrateMRI_10mm_bothCTMRI = allPts_scar_10mm_bothCTMRI + allPts_greyZone_10mm_bothCTMRI;
allPts_normalMRI_10mm_bothCTMRI = allPts_normalMRI_10mm(both_CTMRI_close);
allPts_inFAT_10mm_bothCTMRI = allPts_inFAT_10mm(both_CTMRI_close);
allPts_admixture_10mm_bothCTMRI = allPts_admixture_10mm(both_CTMRI_close);
allPts_substrateCT_10mm_bothCTMRI = allPts_inFAT_10mm_bothCTMRI + allPts_admixture_10mm_bothCTMRI;
allPts_normalCT_10mm_bothCTMRI = allPts_normalCT_10mm(both_CTMRI_close);

allPts_scar_15mm_bothCTMRI = allPts_scar_15mm(both_CTMRI_close);
allPts_greyZone_15mm_bothCTMRI = allPts_greyZone_15mm(both_CTMRI_close);
allPts_substrateMRI_15mm_bothCTMRI = allPts_scar_15mm_bothCTMRI + allPts_greyZone_15mm_bothCTMRI;
allPts_normalMRI_15mm_bothCTMRI = allPts_normalMRI_15mm(both_CTMRI_close);
allPts_inFAT_15mm_bothCTMRI = allPts_inFAT_15mm(both_CTMRI_close);
allPts_admixture_15mm_bothCTMRI = allPts_admixture_15mm(both_CTMRI_close);
allPts_substrateCT_15mm_bothCTMRI = allPts_inFAT_15mm_bothCTMRI + allPts_admixture_15mm_bothCTMRI;
allPts_normalCT_15mm_bothCTMRI = allPts_normalCT_15mm(both_CTMRI_close);

allPts_bipolar_bothCTMRI = allPts_bipolarCT(both_CTMRI_close);
allPts_unipolar_bothCTMRI = allPts_unipolarCT(both_CTMRI_close); % bipolar and unipolar
bipolarCTMRI = allPts_bipolar_bothCTMRI; bipolarCTMRI(bipolarCTMRI>1.5) = 1.5; % Cutoffs from literature
unipolarCTMRI = allPts_unipolar_bothCTMRI; unipolarCTMRI(unipolarCTMRI>8.3) = 8.3;
dzCTMRI = allPts_dzCT(both_CTMRI_close); % deceleration zones

% Converts the above quantities into percentages
allPts_substrateCT_perc = (allPts_inFAT(CT_close)+allPts_admixture(CT_close))./ ...
                        (allPts_inFAT(CT_close)+allPts_admixture(CT_close)+allPts_normalCT(CT_close));
allPts_substrateMRI_perc = (allPts_scar(MRI_close)+allPts_greyZone(MRI_close))./ ...
                        (allPts_scar(MRI_close)+allPts_greyZone(MRI_close)+allPts_normalMRI(MRI_close));
allPts_inFAT_perc = (allPts_inFAT(CT_close))./ ...
                        (allPts_inFAT(CT_close)+allPts_admixture(CT_close)+allPts_normalCT(CT_close));
allPts_scar_perc = (allPts_scar(MRI_close))./ ...
                        (allPts_scar(MRI_close)+allPts_greyZone(MRI_close)+allPts_normalMRI(MRI_close));
allPts_admixture_perc = (allPts_admixture(CT_close))./ ...
                        (allPts_inFAT(CT_close)+allPts_admixture(CT_close)+allPts_normalCT(CT_close));
allPts_greyZone_perc = (allPts_greyZone(MRI_close))./ ...
                        (allPts_scar(MRI_close)+allPts_greyZone(MRI_close)+allPts_normalMRI(MRI_close));
                    
allPts_substrateCT_10mm_perc = (allPts_inFAT_10mm(CT_close)+allPts_admixture_10mm(CT_close))./ ...
                        (allPts_inFAT_10mm(CT_close)+allPts_admixture_10mm(CT_close)+allPts_normalCT_10mm(CT_close));
allPts_substrateMRI_10mm_perc = (allPts_scar_10mm(MRI_close)+allPts_greyZone_10mm(MRI_close))./ ...
                        (allPts_scar_10mm(MRI_close)+allPts_greyZone_10mm(MRI_close)+allPts_normalMRI_10mm(MRI_close));
allPts_inFAT_10mm_perc = (allPts_inFAT_10mm(CT_close))./ ...
                        (allPts_inFAT_10mm(CT_close)+allPts_admixture_10mm(CT_close)+allPts_normalCT_10mm(CT_close));
allPts_scar_10mm_perc = (allPts_scar_10mm(MRI_close))./ ...
                        (allPts_scar_10mm(MRI_close)+allPts_greyZone_10mm(MRI_close)+allPts_normalMRI_10mm(MRI_close));
allPts_admixture_10mm_perc = (allPts_admixture_10mm(CT_close))./ ...
                        (allPts_inFAT_10mm(CT_close)+allPts_admixture_10mm(CT_close)+allPts_normalCT_10mm(CT_close));
allPts_greyZone_10mm_perc = (allPts_greyZone_10mm(MRI_close))./ ...
                        (allPts_scar_10mm(MRI_close)+allPts_greyZone_10mm(MRI_close)+allPts_normalMRI_10mm(MRI_close));
             
allPts_substrateCT_15mm_perc = (allPts_inFAT_15mm(CT_close)+allPts_admixture_15mm(CT_close))./ ...
                        (allPts_inFAT_15mm(CT_close)+allPts_admixture_15mm(CT_close)+allPts_normalCT_15mm(CT_close));
allPts_substrateMRI_15mm_perc = (allPts_scar_15mm(MRI_close)+allPts_greyZone_15mm(MRI_close))./ ...
                        (allPts_scar_15mm(MRI_close)+allPts_greyZone_15mm(MRI_close)+allPts_normalMRI_15mm(MRI_close));
allPts_inFAT_15mm_perc = (allPts_inFAT_15mm(CT_close))./ ...
                        (allPts_inFAT_15mm(CT_close)+allPts_admixture_15mm(CT_close)+allPts_normalCT_15mm(CT_close));
allPts_scar_15mm_perc = (allPts_scar_15mm(MRI_close))./ ...
                        (allPts_scar_15mm(MRI_close)+allPts_greyZone_15mm(MRI_close)+allPts_normalMRI_15mm(MRI_close));
allPts_admixture_15mm_perc = (allPts_admixture_15mm(CT_close))./ ...
                        (allPts_inFAT_15mm(CT_close)+allPts_admixture_15mm(CT_close)+allPts_normalCT_15mm(CT_close));
allPts_greyZone_15mm_perc = (allPts_greyZone_15mm(MRI_close))./ ...
                        (allPts_scar_15mm(MRI_close)+allPts_greyZone_15mm(MRI_close)+allPts_normalMRI_15mm(MRI_close));

% Grouping the variables
allPts_substrateCT = (allPts_inFAT(CT_close)+allPts_admixture(CT_close));
allPts_substrateMRI = (allPts_scar(MRI_close)+allPts_greyZone(MRI_close));
allPts_inFAT = (allPts_inFAT(CT_close));
allPts_scar = (allPts_scar(MRI_close));
allPts_admixture = (allPts_admixture(CT_close));
allPts_greyZone = (allPts_greyZone(MRI_close));
                    
% Pull out the corresponding EAM variables to compare to the imaging
% variables
bipolarCT = allPts_bipolarCT(CT_close); bipolarCT(bipolarCT>1.5) = 1.5;
bipolarMRI = allPts_bipolarMRI(MRI_close); bipolarMRI(bipolarMRI>1.5) = 1.5;
unipolarCT = allPts_unipolarCT(CT_close); unipolarCT(unipolarCT>8.3) = 8.3;
unipolarMRI = allPts_unipolarMRI(MRI_close); unipolarMRI(unipolarMRI>8.3) = 8.3;
dzCT = allPts_dzCT(CT_close);
dzMRI = allPts_dzMRI(MRI_close);

%%% Percentage correlations
% Convert above absolute quantities into percentages for all depths
% considered
allPts_scar_perc_bothCTMRI = allPts_scar_bothCTMRI./...
                (allPts_scar_bothCTMRI+allPts_greyZone_bothCTMRI+allPts_normalMRI_bothCTMRI);
allPts_greyZone_perc_bothCTMRI = allPts_greyZone_bothCTMRI./...
                (allPts_scar_bothCTMRI+allPts_greyZone_bothCTMRI+allPts_normalMRI_bothCTMRI);
allPts_substrateMRI_perc_bothCTMRI = (allPts_scar_bothCTMRI+allPts_greyZone_bothCTMRI)./...
                (allPts_scar_bothCTMRI+allPts_greyZone_bothCTMRI+allPts_normalMRI_bothCTMRI);
allPts_normalMRI_perc_bothCTMRI = allPts_normalMRI_bothCTMRI./...
                (allPts_scar_bothCTMRI+allPts_greyZone_bothCTMRI+allPts_normalMRI_bothCTMRI);
allPts_inFAT_perc_bothCTMRI = allPts_inFAT_bothCTMRI./...
                (allPts_inFAT_bothCTMRI+allPts_admixture_bothCTMRI+allPts_normalCT_bothCTMRI);
allPts_admixture_perc_bothCTMRI = allPts_admixture_bothCTMRI./...
                (allPts_inFAT_bothCTMRI+allPts_admixture_bothCTMRI+allPts_normalCT_bothCTMRI);
allPts_substrateCT_perc_bothCTMRI = (allPts_inFAT_bothCTMRI+allPts_admixture_bothCTMRI)./...
                (allPts_inFAT_bothCTMRI+allPts_admixture_bothCTMRI+allPts_normalCT_bothCTMRI);
allPts_normalCT_perc_bothCTMRI = allPts_normalCT_bothCTMRI./...
                (allPts_inFAT_bothCTMRI+allPts_admixture_bothCTMRI+allPts_normalCT_bothCTMRI);
            
allPts_scar_10mm_perc_bothCTMRI = allPts_scar_10mm_bothCTMRI./...
                (allPts_scar_10mm_bothCTMRI+allPts_greyZone_10mm_bothCTMRI+allPts_normalMRI_10mm_bothCTMRI);
allPts_greyZone_10mm_perc_bothCTMRI = allPts_greyZone_10mm_bothCTMRI./...
                (allPts_scar_10mm_bothCTMRI+allPts_greyZone_10mm_bothCTMRI+allPts_normalMRI_10mm_bothCTMRI);
allPts_substrateMRI_10mm_perc_bothCTMRI = (allPts_scar_10mm_bothCTMRI+allPts_greyZone_10mm_bothCTMRI)./...
                (allPts_scar_10mm_bothCTMRI+allPts_greyZone_10mm_bothCTMRI+allPts_normalMRI_10mm_bothCTMRI);
allPts_normalMRI_10mm_perc_bothCTMRI = allPts_normalMRI_10mm_bothCTMRI./...
                (allPts_scar_10mm_bothCTMRI+allPts_greyZone_10mm_bothCTMRI+allPts_normalMRI_10mm_bothCTMRI);
allPts_inFAT_10mm_perc_bothCTMRI = allPts_inFAT_10mm_bothCTMRI./...
                (allPts_inFAT_10mm_bothCTMRI+allPts_admixture_10mm_bothCTMRI+allPts_normalCT_10mm_bothCTMRI);
allPts_admixture_10mm_perc_bothCTMRI = allPts_admixture_10mm_bothCTMRI./...
                (allPts_inFAT_10mm_bothCTMRI+allPts_admixture_10mm_bothCTMRI+allPts_normalCT_10mm_bothCTMRI);
allPts_substrateCT_10mm_perc_bothCTMRI = (allPts_inFAT_10mm_bothCTMRI+allPts_admixture_10mm_bothCTMRI)./...
                (allPts_inFAT_10mm_bothCTMRI+allPts_admixture_10mm_bothCTMRI+allPts_normalCT_10mm_bothCTMRI);
allPts_normalCT_10mm_perc_bothCTMRI = allPts_normalCT_bothCTMRI./...
                (allPts_inFAT_10mm_bothCTMRI+allPts_admixture_10mm_bothCTMRI+allPts_normalCT_10mm_bothCTMRI);
            
allPts_scar_15mm_perc_bothCTMRI = allPts_scar_15mm_bothCTMRI./...
                (allPts_scar_15mm_bothCTMRI+allPts_greyZone_15mm_bothCTMRI+allPts_normalMRI_15mm_bothCTMRI);
allPts_greyZone_15mm_perc_bothCTMRI = allPts_greyZone_15mm_bothCTMRI./...
                (allPts_scar_15mm_bothCTMRI+allPts_greyZone_15mm_bothCTMRI+allPts_normalMRI_15mm_bothCTMRI);
allPts_substrateMRI_15mm_perc_bothCTMRI = (allPts_scar_15mm_bothCTMRI+allPts_greyZone_15mm_bothCTMRI)./...
                (allPts_scar_15mm_bothCTMRI+allPts_greyZone_15mm_bothCTMRI+allPts_normalMRI_15mm_bothCTMRI);
allPts_normalMRI_15mm_perc_bothCTMRI = allPts_normalMRI_15mm_bothCTMRI./...
                (allPts_scar_15mm_bothCTMRI+allPts_greyZone_15mm_bothCTMRI+allPts_normalMRI_15mm_bothCTMRI);
allPts_inFAT_15mm_perc_bothCTMRI = allPts_inFAT_15mm_bothCTMRI./...
                (allPts_inFAT_15mm_bothCTMRI+allPts_admixture_15mm_bothCTMRI+allPts_normalCT_15mm_bothCTMRI);
allPts_admixture_15mm_perc_bothCTMRI = allPts_admixture_15mm_bothCTMRI./...
                (allPts_inFAT_15mm_bothCTMRI+allPts_admixture_15mm_bothCTMRI+allPts_normalCT_15mm_bothCTMRI);
allPts_substrateCT_15mm_perc_bothCTMRI = (allPts_inFAT_15mm_bothCTMRI+allPts_admixture_15mm_bothCTMRI)./...
                (allPts_inFAT_15mm_bothCTMRI+allPts_admixture_15mm_bothCTMRI+allPts_normalCT_15mm_bothCTMRI);
allPts_normalCT_15mm_perc_bothCTMRI = allPts_normalCT_15mm_bothCTMRI./...
                (allPts_inFAT_15mm_bothCTMRI+allPts_admixture_15mm_bothCTMRI+allPts_normalCT_15mm_bothCTMRI);


%%% Do areas of lower voltage typically consist of inFAT and scar? or just scar?
% Corresponds to Figure 2A second panel
only_scar_bV = mean(bipolarCTMRI(allPts_substrateCT_perc_bothCTMRI==0&allPts_substrateMRI_perc_bothCTMRI>0));
fat_and_scar_bV = mean(bipolarCTMRI(allPts_substrateCT_perc_bothCTMRI>0&allPts_substrateMRI_perc_bothCTMRI>0));
only_scar_uV = mean(unipolarCTMRI(allPts_substrateCT_perc_bothCTMRI==0&allPts_substrateMRI_perc_bothCTMRI>0));
fat_and_scar_uV = mean(unipolarCTMRI(allPts_substrateCT_perc_bothCTMRI>0&allPts_substrateMRI_perc_bothCTMRI>0));

%-----------------------------------%
%% Section 3: Look at the different regions as defined by traditional voltage cutoffs
% This tells us dense scar is commonly comprised of both fat AND scar
% Rarely ever fat alone, and only some of the time scar alone
depth_tags = {'','_10mm','_15mm'};
lowestBiVolt_fat_and_scar_perc = [];
lowestBiVolt_only_scar_perc = [];
lowestBiVolt_only_fat_perc = [];
lowestBiVolt_none_perc = [];
bz_Bi_fat_and_scar_perc = [];
bz_Bi_only_scar_perc = [];
bz_Bi_only_fat_perc = [];
bz_Bi_only_fat_perc = [];
lowestUniVolt_fat_and_scar_perc = [];
lowestUniVolt_only_scar_perc = [];
lowestUniVolt_only_fat_perc = [];
lowestUniVolt_none_perc = [];
bz_Uni_fat_and_scar_perc = [];
bz_Uni_only_scar_perc = [];
bz_Uni_only_fat_perc = [];
bz_Uni_none_perc = [];
T = 0.1; % Threshold for considering a point to have fat or scar
for d = 1:length(depth_tags)
    var_scar = eval(['allPts_substrateMRI' depth_tags{d} '_perc_bothCTMRI']);
    var_fat = eval(['allPts_substrateCT' depth_tags{d} '_perc_bothCTMRI']);
    lowestBiVolt_fat_and_scar_perc(d) = sum((var_scar>T)&(bipolarCTMRI<=0.5)&...
        (var_fat>T))/sum(bipolarCTMRI<=0.5);
    lowestBiVolt_only_scar_perc(d) = sum((var_scar>T)&(bipolarCTMRI<=0.5)&...
        (var_fat<=T))/sum(bipolarCTMRI<=0.5);
    lowestBiVolt_only_fat_perc(d) = sum((var_scar<=T)&(bipolarCTMRI<=0.5)&...
        (var_fat>T))/sum(bipolarCTMRI<=0.5);
    lowestBiVolt_none_perc(d) = sum((var_scar<=T)&(bipolarCTMRI<=0.5)&...
        (var_fat<=T))/sum(bipolarCTMRI<=0.5);
    bz_Bi_fat_and_scar_perc(d) = sum((var_scar>T)&(bipolarCTMRI<1.5)&(bipolarCTMRI>0.5)&...
        (var_fat>T))/sum((bipolarCTMRI<1.5)&(bipolarCTMRI>0.5));
    bz_Bi_only_scar_perc(d) = sum((var_scar>T)&(bipolarCTMRI<1.5)&(bipolarCTMRI>0.5)&...
        (var_fat<=T))/sum((bipolarCTMRI<1.5)&(bipolarCTMRI>0.5));
    bz_Bi_only_fat_perc(d) = sum((var_scar<=T)&(bipolarCTMRI<1.5)&(bipolarCTMRI>0.5)&...
        (var_fat>T))/sum((bipolarCTMRI<1.5)&(bipolarCTMRI>0.5));
    bz_Bi_none_perc(d) = sum((var_scar<=T)&(bipolarCTMRI<1.5)&(bipolarCTMRI>0.5)&...
        (var_fat<=T))/sum((bipolarCTMRI<1.5)&(bipolarCTMRI>0.5));

    lowestUniVolt_fat_and_scar_perc(d) = sum((var_scar>T)&(unipolarCTMRI<=3.3)&...
        (var_fat>T))/sum(unipolarCTMRI<=3.3);
    lowestUniVolt_only_scar_perc(d) = sum((var_scar>T)&(unipolarCTMRI<=3.3)&...
        (var_fat<=T))/sum(unipolarCTMRI<=3.3);
    lowestUniVolt_only_fat_perc(d) = sum((var_scar<=T)&(unipolarCTMRI<=3.3)&...
        (var_fat>T))/sum(unipolarCTMRI<=3.3);
    lowestUniVolt_none_perc(d) = sum((var_scar<=T)&(unipolarCTMRI<=3.3)&...
        (var_fat<=T))/sum(unipolarCTMRI<=3.3);
    bz_Uni_fat_and_scar_perc(d) = sum((var_scar>T)&(unipolarCTMRI<8.3)&(unipolarCTMRI>3.3)&...
        (var_fat>T))/sum((unipolarCTMRI<8.3)&(unipolarCTMRI>3.3));
    bz_Uni_only_scar_perc(d) = sum((var_scar>T)&(unipolarCTMRI<8.3)&(unipolarCTMRI>3.3)&...
        (var_fat<=T))/sum((unipolarCTMRI<8.3)&(unipolarCTMRI>3.3));
    bz_Uni_only_fat_perc(d) = sum((var_scar<=T)&(unipolarCTMRI<8.3)&(unipolarCTMRI>3.3)&...
        (var_fat>T))/sum((unipolarCTMRI<8.3)&(unipolarCTMRI>3.3));
    bz_Uni_none_perc(d) = sum((var_scar<=T)&(unipolarCTMRI<8.3)&(unipolarCTMRI>3.3)&...
        (var_fat<=T))/sum((unipolarCTMRI<8.3)&(unipolarCTMRI>3.3));
end
figure()
bar([lowestBiVolt_fat_and_scar_perc; lowestBiVolt_only_scar_perc; ...
    lowestBiVolt_only_fat_perc; lowestBiVolt_none_perc]','stacked')
title('Dense Regions Defined by Bipolar Voltage','fontsize',14)
ylim([0,1])

figure()
bar([bz_Bi_fat_and_scar_perc; bz_Bi_only_scar_perc; ...
    bz_Bi_only_fat_perc; bz_Bi_none_perc]','stacked')
title('Border Zone Regions Defined by Bipolar Voltage','fontsize',14)
ylim([0,1])

figure()
bar([lowestUniVolt_fat_and_scar_perc; lowestUniVolt_only_scar_perc; ...
    lowestUniVolt_only_fat_perc; lowestUniVolt_none_perc]','stacked')
title('Dense Regions Defined by Unipolar Voltage','fontsize',14)
ylim([0,1])

figure()
bar([bz_Uni_fat_and_scar_perc; bz_Uni_only_scar_perc; ...
    bz_Uni_only_fat_perc; bz_Uni_none_perc]','stacked')
title('Border Zone Regions Defined by Unipolar Voltage','fontsize',14)
ylim([0,1])

% Make plots only with the 10 mm radius
lbv = [lowestBiVolt_fat_and_scar_perc; lowestBiVolt_only_scar_perc; ...
    lowestBiVolt_only_fat_perc; lowestBiVolt_none_perc]'; % All lowest bipolar voltage
luv = [lowestUniVolt_fat_and_scar_perc; lowestUniVolt_only_scar_perc; ...
    lowestUniVolt_only_fat_perc; lowestUniVolt_none_perc]'; % All lowest unipolar voltage
bz_bv = [bz_Bi_fat_and_scar_perc; bz_Bi_only_scar_perc; ...
    bz_Bi_only_fat_perc; bz_Bi_none_perc]'; % Border zone by bipolar voltage
bz_uv = [bz_Uni_fat_and_scar_perc; bz_Uni_only_scar_perc; ...
    bz_Uni_only_fat_perc; bz_Uni_none_perc]'; % Border zone by unipolar voltage

figure()
bar(categorical({'Dense BiV','Dense UniV','BZ BiV','BZ UniV'}'),...
    [lbv(2,:); luv(2,:); bz_bv(2,:); bz_uv(2,:)],'stacked')
title('EAM Regions by Voltage amplitude','fontsize',14)
ylabel('Percentage Breakdown','fontsize',12);
legend({'inFAT and Scar','Scar only','inFAT only','No remodeling'})
ylim([0,1])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot voltage violin plots
T = 0.1;
MRI1 = allPts_substrateMRI_10mm_perc_bothCTMRI;
CT1 = allPts_substrateCT_10mm_perc_bothCTMRI;
fat_and_scar_biV = bipolarCTMRI(MRI1>T&CT1>T);
fat_only_biV = bipolarCTMRI(MRI1<=T&CT1>T);
scar_only_biV = bipolarCTMRI(MRI1>T&CT1<=T);
noRemodel_biV = bipolarCTMRI(MRI1<=T&CT1<=T);

LAVA_in_fat = CT1(LAVA_bothCTMRI)>T;
LAVA_in_scar = MRI1(LAVA_bothCTMRI)>T;
noLAVA_in_fat = CT1(~LAVA_bothCTMRI)>T;
noLAVA_in_scar = MRI1(~LAVA_bothCTMRI)>T;
tbl_for_fisher = table([sum(LAVA_in_fat); sum(LAVA_in_scar)],[sum(noLAVA_in_fat); sum(noLAVA_in_scar)],...
    'VariableNames',{'LAVA','No LAVA'},'RowNames',{'Fat','Scar'});
[hh pval stats] = fishertest(tbl_for_fisher);

fat_and_scar_uniV = unipolarCTMRI(MRI1>T&CT1>T);
fat_only_uniV = unipolarCTMRI(MRI1<=T&CT1>T);
scar_only_uniV = unipolarCTMRI(MRI1>T&CT1<=T);
noRemodel_uniV = unipolarCTMRI(MRI1<=T&CT1<=T);

BiV_x = [mean(fat_and_scar_biV); mean(scar_only_biV); ...
    mean(fat_only_biV); mean(noRemodel_biV)];
stdBiV = [std(fat_and_scar_biV); std(scar_only_biV); ...
    std(fat_only_biV); std(noRemodel_biV)];
UniV_x = [mean(fat_and_scar_uniV); mean(scar_only_uniV); ...
    mean(fat_only_uniV); mean(noRemodel_uniV)];
stdUniV = [std(fat_and_scar_uniV); std(scar_only_uniV); ...
    std(fat_only_uniV); std(noRemodel_uniV)];

figure(); % Bipolar voltage distribution first
% fat and scar, scar only, fat only, no remodeling
bar([BiV_x],'stacked')
hold on; errorbar(BiV_x,stdBiV,'k','LineStyle','none'); hold off
ylim([0 2])
title('Bipolar Voltage for each region')

figure(); % Unipolar voltage distribution first
% fat and scar, scar only, fat only, no remodeling
bar([UniV_x],'stacked')
hold on; errorbar(UniV_x,stdUniV,'k','LineStyle','none'); hold off
ylim([0 9])
title('Unipolar Voltage for each region')

% Anova test to look at differences between bipolar voltage for the
% different sections
bb = [(fat_and_scar_biV); (scar_only_biV); ...
    (fat_only_biV); (noRemodel_biV)];
b1 = fat_and_scar_biV; b2 = scar_only_biV; b3 = fat_only_biV; b4 = noRemodel_biV;
lb = cumsum([length(b1);length(b2);length(b3);length(b4)]);
lb1 = [1;lb(1:end-1)+1];
tags_b = zeros(length(bb),1);for i = 1:4; tags_b(lb1(i):lb(i)) = i; end
[pp tbl stats] = anova1(bb,tags_b); % Assess for differences between different distributions
multcompare(stats) % unbalanced anova tests

% Anova test to look at differences between unipolar voltage for the
% different sections
uu = [(fat_and_scar_uniV); (scar_only_uniV); ...
    (fat_only_uniV); (noRemodel_uniV)];
u1 = fat_and_scar_uniV; u2 = scar_only_uniV; u3 = fat_only_uniV; u4 = noRemodel_uniV;
lu = cumsum([length(u1);length(u2);length(u3);length(u4)]);
lu1 = [1;lu(1:end-1)+1];
tags_u = zeros(length(uu),1);for i = 1:4; tags_u(lu1(i):lu(i)) = i; end
[pp tbl stats] = anova1(uu,tags_u); % Assess for differences between different distributions
multcompare(stats) % unbalanced anova tests

%% Section 4: Next question: What is the composition of substrate in deceleration zone
% regions
depth_tags = {'','_10mm','_15mm'};
DZ_fat_and_scar_perc = [];
DZ_only_scar_perc = [];
DZ_only_fat_perc = [];
DZ_none_perc = [];
nonDZ_fat_and_scar_perc = [];
nonDZ_only_scar_perc = [];
nonDZ_Bi_only_fat_perc = [];
nonDZ_Bi_none_perc = [];

h_ks = []; p_ks = [];
dz_depths_fs = []; % Mean of deceleration zones
dz_stds_fs = []; % STD of deceleration zones
dz_depths_s = [];
dz_stds_s = [];

dz_cu = 5; % Cutoff for deceleration zone versus not
s_cu = 0.1; % Cut off % for substrate to be considered inFAT or not
for d = 1:length(depth_tags)
    var_scar = eval(['allPts_substrateMRI' depth_tags{d} '_perc_bothCTMRI']);
    var_fat = eval(['allPts_substrateCT' depth_tags{d} '_perc_bothCTMRI']);
    DZ_fat_and_scar_perc(d) = sum((var_scar>s_cu)&(dzCTMRI>=dz_cu)&...
        (var_fat>s_cu))/sum(dzCTMRI>=dz_cu);
    DZ_only_scar_perc(d) = sum((var_scar>s_cu)&(dzCTMRI>=dz_cu)&...
        (var_fat<=s_cu))/sum(dzCTMRI>=dz_cu);
    DZ_only_fat_perc(d) = sum((var_scar<=s_cu)&(dzCTMRI>=dz_cu)&...
        (var_fat>s_cu))/sum(dzCTMRI>=dz_cu);
    DZ_none_perc(d) = sum((var_scar<=s_cu)&(dzCTMRI>=dz_cu)&...
        (var_fat<=s_cu))/sum(dzCTMRI>=dz_cu);
    nonDZ_fat_and_scar_perc(d) = sum((var_scar>s_cu)&(dzCTMRI<dz_cu)&...
        (var_fat>s_cu))/sum(dzCTMRI<dz_cu);
    nonDZ_only_scar_perc(d) = sum((var_scar>s_cu)&(dzCTMRI<dz_cu)&...
        (var_fat<=s_cu))/sum(dzCTMRI<dz_cu);
    nonDZ_only_fat_perc(d) = sum((var_scar<=s_cu)&(dzCTMRI<dz_cu)&...
        (var_fat>s_cu))/sum(dzCTMRI<dz_cu);
    nonDZ_none_perc(d) = sum((var_scar<=s_cu)&(dzCTMRI<dz_cu)&...
        (var_fat<=s_cu))/sum(dzCTMRI<dz_cu);
    [hh,pval] = kstest2(dzCTMRI(var_scar>s_cu&var_fat>s_cu),...
                        dzCTMRI(var_scar>s_cu&var_fat<=s_cu));
    h_ks(d) = hh; p_ks(d) = pval;
    dz_depths_fs(d) =  mean(dzCTMRI(var_scar>s_cu&var_fat>s_cu));
    dz_depths_s(d) =  mean(dzCTMRI(var_scar>s_cu&var_fat<=s_cu));
    dz_stds_fs(d) =  std(dzCTMRI(var_scar>s_cu&var_fat>s_cu));
    dz_stds_s(d) =  std(dzCTMRI(var_scar>s_cu&var_fat<=s_cu));
end

figure()
bar([DZ_fat_and_scar_perc; DZ_only_scar_perc; ...
    DZ_only_fat_perc; DZ_none_perc]','stacked')
title('Distribution Deceleration Zones','fontsize',14)
ylim([0,1])

figure()
bar([nonDZ_fat_and_scar_perc; nonDZ_only_scar_perc; ...
    nonDZ_only_fat_perc; nonDZ_none_perc]','stacked')
title('Distribution Non Deceleration Zones','fontsize',14)
ylim([0,1])

% Create a pie chart for the distribution of deceleration zones at a depth
% of 10 mm
d = 2;
figure();pie([DZ_fat_and_scar_perc(d); DZ_only_scar_perc(d); ...
DZ_only_fat_perc(d); DZ_none_perc(d)])

title('Distribution of Deceleration zones at a 10 mm radius')
legend('fat and scar','scar only','fat only','none')
% Create a box plot for the isochronal crowding between fat/scar and scar
% only
s_cu = 0.1;
var_scar = eval(['allPts_substrateMRI' depth_tags{d} '_perc_bothCTMRI']);
var_fat = eval(['allPts_substrateCT' depth_tags{d} '_perc_bothCTMRI']);
x1 = dzCTMRI(var_scar>s_cu&var_fat>s_cu); % scar and fat
x2 = dzCTMRI(var_scar>s_cu&var_fat<=s_cu); % Number of isocrhones scar no fat
x3 = dzCTMRI(var_scar<=s_cu&var_fat>s_cu); % fat and no scar
x4 = dzCTMRI(var_scar<=s_cu&var_fat<=s_cu); % no remodeling
x = [x1;x2;x3;x4];
g1 = repmat({'Scar with inFAT'},length(x1),1);
g2 = repmat({'Scar without inFAT'},length(x2),1);
g3 = repmat({'inFAT no Scar'},length(x3),1);
g4 = repmat({'No remodeling'},length(x4),1);
g = [g1;g2;g3;g4];
lx = cumsum([length(x1);length(x2);length(x3);length(x4)]);
lx1 = [1;lx(1:end-1)+1];
tags = zeros(length(x),1);for i = 1:4; tags(lx1(i):lx(i)) = i; end
[pp tbl stats] = anova1(x,tags); % Assess for differences between different distributions
multcompare(stats)

boxplot(x,g)
title('Differences in Isochronal Crowding')

% Alternative way to plot using violin plots
% Requires downloading violin.m from MATLAB central exchange
figure();
violin({x1,x2,x3,x4},'bw',.5,'facecolor',[0.3 0.8 0.3; 0 0.85 0; 0 0.2 0; 0.8 0.8 0.8])

