% INFINITY_compareFatandScar_byAHA_onlyAnalysis.m
% Goes through the AHA segments of each CT and MRI pair and looks at the
% difference in structural remodeling distributions


%% Load in data from INFINITY_substrateInfo.mat
load('INFINITY_substrateInfo.mat')
% Define AHA segments with respect to these triangles (follows from
% UVC-like definition)
AHA = cell(16,1);
AHA{1} = [149:154,185:190,221:226];
AHA{2} = [155:160,191:196,227:232];
AHA{3} = [161:166,197:202,233:238];
AHA{4} = [131:136,167:172,203:208];
AHA{5} = [137:142,173:178,209:214];
AHA{6} = [143:148,179:184,215:220];
AHA{7} = [44:48,77:82,113:118];
AHA{8} = [49:53,83:88,119:124];
AHA{9} = [54:58,89:94,125:130];
AHA{10} = [29:33,59:64,95:100];
AHA{11} = [34:38,65:70,101:106];
AHA{12} = [39:43,71:76,107:112];
AHA{13} = [5:6,21:24];
AHA{14} = [7:8,25:28];
AHA{15} = [3:4,15:20];
AHA{16} = [1:2,9:14];


inFAT = zeros(length(CTdata),238); admixture = inFAT; normalCT = inFAT;
scar = inFAT; greyZone = inFAT; normalMRI = inFAT;
for l = 1:length(CTdata)
    inFAT(l,:) = CTdata{l}.inFAT;
    admixture(l,:) = CTdata{l}.admixture;
    normalCT(l,:) = CTdata{l}.normal;
    scar(l,:) = MRIdata{l}.scar;
    greyZone(l,:) = MRIdata{l}.greyZone;
    normalMRI(l,:) = MRIdata{l}.normal;
end
figure(); hold on;colors = linspecer(238);
for i = 1:238;plot(log(inFAT(:,i)),log(scar(:,i)),'Marker','.','LineStyle','none','Color',colors(i,:));end

% Re-arrange segments into AHA segments
inFAT_AHA = zeros(length(CTdata),16); admixture_AHA = inFAT_AHA; normalCT_AHA = inFAT_AHA;
scar_AHA = inFAT_AHA; greyZone_AHA = inFAT_AHA; normalMRI_AHA = inFAT_AHA;
for j = 1:16
    inFAT_AHA(:,j) = sum(inFAT(:,AHA{j}),2);
    admixture_AHA(:,j) = sum(admixture(:,AHA{j}),2);
    normalCT_AHA(:,j) = sum(normalCT(:,AHA{j}),2);
    scar_AHA(:,j) = sum(scar(:,AHA{j}),2);
    greyZone_AHA(:,j) = sum(greyZone(:,AHA{j}),2);
    normalMRI_AHA(:,j) = sum(normalMRI(:,AHA{j}),2);    
end

% Re-arrange AHA segments into anatomical segments
inFAT_anatomical = zeros(length(CTdata),4); admixture_anatomical = inFAT_anatomical; 
normalCT_anatomical = inFAT_anatomical;scar_anatomical= inFAT_anatomical; 
greyZone_anatomical = inFAT_anatomical; 
normalMRI_anatomical = inFAT_anatomical;
anatomical_segs = cell(4,1);
anatomical_segs{1} = [2 3 8 9];%[2,3,8,9];
anatomical_segs{2} = [13:16];
anatomical_segs{3} = [1 6 7 12];%[1 6 7 12];
anatomical_segs{4} = [4 5 10 11];%[4 5 10 11];
for j = 1:4
    inFAT_anatomical(:,j) = sum(inFAT_AHA(:,anatomical_segs{j}),2);
    admixture_anatomical(:,j) = sum(admixture_AHA(:,anatomical_segs{j}),2);
    normalCT_anatomical(:,j) = sum(normalCT_AHA(:,anatomical_segs{j}),2);
    scar_anatomical(:,j) = sum(scar_AHA(:,anatomical_segs{j}),2);
    greyZone_anatomical(:,j) = sum(greyZone_AHA(:,anatomical_segs{j}),2);
    normalMRI_anatomical(:,j) = sum(normalMRI_AHA(:,anatomical_segs{j}),2);    
end

z_segs = cell(3,1);
z_segs{1} = [1:6];
z_segs{2} = [7:12];
z_segs{3} = [13:16];
inFAT_z = zeros(length(CTdata),3);
admixture_z = zeros(length(CTdata),3);
normalCT_z = zeros(length(CTdata),3);
scar_z = zeros(length(CTdata),3);
greyZone_z = zeros(length(CTdata),3);
normalMRI_z = zeros(length(CTdata),3);
for j = 1:3
    inFAT_z(:,j) = sum(inFAT_AHA(:,z_segs{j}),2);
    admixture_z(:,j) = sum(admixture_AHA(:,z_segs{j}),2);
    normalCT_z(:,j) = sum(normalCT_AHA(:,z_segs{j}),2);
    scar_z(:,j) = sum(scar_AHA(:,z_segs{j}),2);
    greyZone_z(:,j) = sum(greyZone_AHA(:,z_segs{j}),2);
    normalMRI_z(:,j) = sum(normalMRI_AHA(:,z_segs{j}),2);   
end

bothaz_segs = cell(7,1);
bothaz_segs{1} = [2:3]; % basal septum
bothaz_segs{2} = [1 6]; % basal anterior/anterolateral
bothaz_segs{3} = [4 5]; % basal inferior/inferolateral
bothaz_segs{4} = [8 9]; % Mid septum
bothaz_segs{5} = [7 12]; % Mid anterior/anterolateral
bothaz_segs{6} = [10 11]; % Mid inferior/inferolateral
bothaz_segs{7} = [13:16]; % Apex
inFAT_bothaz = zeros(length(CTdata),7);
admixture_bothaz = zeros(length(CTdata),7);
normalCT_bothaz = zeros(length(CTdata),7);
scar_bothaz = zeros(length(CTdata),7);
greyZone_bothaz = zeros(length(CTdata),7);
normalMRI_bothaz = zeros(length(CTdata),7);
for j = 1:7
    inFAT_bothaz(:,j) = sum(inFAT_AHA(:,bothaz_segs{j}),2);
    admixture_bothaz(:,j) = sum(admixture_AHA(:,bothaz_segs{j}),2);
    normalCT_bothaz(:,j) = sum(normalCT_AHA(:,bothaz_segs{j}),2);
    scar_bothaz(:,j) = sum(scar_AHA(:,bothaz_segs{j}),2);
    greyZone_bothaz(:,j) = sum(greyZone_AHA(:,bothaz_segs{j}),2);
    normalMRI_bothaz(:,j) = sum(normalMRI_AHA(:,bothaz_segs{j}),2);   
end

% Compare total amount of fat and scar per patient
[rho_val,p_val] = corr(sum(scar,2)*.035^3,sum(inFAT,2)*.035^3);
mdl = fitlm(sum(scar,2)*.035^3,sum(inFAT,2)*.035^3);
Xdomain = linspace(0,0.035^3*max([max(sum(inFAT,2)),max(sum(scar,2))])+0.1,100);
Ydomain = mdl.Coefficients.Estimate(1) + mdl.Coefficients.Estimate(2)*Xdomain;
figure();hold on; plot(Xdomain,Ydomain,'r-','linewidth',2)
plot(0.035^3*sum(scar,2),0.035^3*sum(inFAT,2),'b.','MarkerSize',20); hold off
xlabel('Total Amount of Scar on LGE-MRI (cm^3)','fontsize',14)
ylabel('Total Amount of inFAT on CE-CT (cm^3)','fontsize',14)
title('Comparing inFAT and Scar per Patient','fontsize',16)
axis tight

%*******************************************%
% Compare total amount of substrate per patient
% We will use this as one of the figures, shows a reasonable trend in the
% amount of substrate
[rho_val2,p_val2] = corr(sum(scar+greyZone,2)*.035^3,sum(inFAT+admixture,2)*.035^3);
mdl = fitlm(1.055*sum(scar+greyZone,2)*.035^3,1.055*sum(inFAT+admixture,2)*.035^3);
Xdomain = linspace(0,1.055*0.035^3*max([max(sum(inFAT+admixture,2)),max(sum(scar+greyZone,2))])+0.1,100);
Ydomain = mdl.Coefficients.Estimate(1) + mdl.Coefficients.Estimate(2)*Xdomain;
figure();hold on; plot(Xdomain,Ydomain,'r-','linewidth',2)
plot(1.055*0.035^3*sum(scar+greyZone,2),1.055*0.035^3*sum(inFAT+admixture,2),'b.','MarkerSize',50); hold off
xlabel('Scar + Grey Zone on LGE-MRI (g)','fontsize',14)
ylabel('inFAT + Admixture on CE-CT (g)','fontsize',14)
title('Comparing Arrhythmogenic Substrate per Patient','fontsize',14)
axis tight

% Compare the substrate by AHA segments
colors = linspecer(16);
p_AHA = zeros(16,1);
figure(); hold on;
for i = 1:16
    plot(log(inFAT_AHA(:,i)),log(scar_AHA(:,i)),'Marker','.','LineStyle','none',...
        'MarkerSize',20,'Color',colors(i,:))
    [hi,pi] = ttest2(log(inFAT_AHA(:,i)+1e-4),log(scar_AHA(:,i)+1e-4));
    p_AHA(i) = pi;
end
legend('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16')
xlabel('Log Amount of Scar on LGE-MRI (cm^3)','fontsize',14)
ylabel('Log Amount of inFAT on CE-CT (cm^3)','fontsize',14)
title('Comparing Scar and inFAT per Patient by AHA segments','fontsize',16)
hold off

% Compare the substrate by AHA segments
colors = linspecer(16);
p_AHA2 = zeros(16,1);
figure(); hold on;
for i = 1:16
    plot(log(inFAT_AHA(:,i)+admixture_AHA(:,i)),log(scar_AHA(:,i)+greyZone_AHA(:,i)),'Marker','.','LineStyle','none',...
        'MarkerSize',20,'Color',colors(i,:))
    [hi,pi] = ttest2(log(inFAT_AHA(:,i)+admixture_AHA(:,i)+1e-4),log(scar_AHA(:,i)+greyZone_AHA(:,i)+1e-4));
    p_AHA2(i) = pi;
end
legend('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16')
xlabel('Log Amount of Scar + Grey Zone on LGE-MRI (cm^3)','fontsize',14)
ylabel('Log Amount of inFAT + Admixture on CE-CT (cm^3)','fontsize',14)
title('Comparing Arrhythmogenic Substrate per Patient by AHA segments','fontsize',16)
hold off

% Compare the substrate by anatomical segments
colors = linspecer(4);
p_AHA = zeros(4,1);
figure(); hold on;
for i = 1:4
    plot(log(inFAT_anatomical(:,i)),log(scar_anatomical(:,i)),'Marker','.','LineStyle','none',...
        'MarkerSize',20,'Color',colors(i,:))
    [hi,pi] = ttest2(log(inFAT_anatomical(:,i)+1e-4),log(scar_anatomical(:,i)+1e-4));
    p_AHA(i) = pi;
end
legend('Septum','Apex','Anterior','Inferior')
xlabel('Log Amount of Scar on LGE-MRI (cm^3)','fontsize',14)
ylabel('Log Amount of inFAT on CE-CT (cm^3)','fontsize',14)
title('Comparing Scar and inFAT per Patient by AHA segments','fontsize',16)
hold off

% Compare the substrate by anatomical segments
colors = linspecer(4);
all_pvals = zeros(4,1);
all_rhovals = all_pvals;
figure();hold on;
for i = 1:4
    [rho_val,p_val] = corr(log(sum(scar_anatomical(:,i),2)*.035^3+1e-4),...
        log(sum(inFAT_anatomical(:,i),2)*.035^3+1e-4));
%     mdl = fitlm(1.055*sum(scar_anatomical(:,i)+greyZone_anatomical(:,i),2)*.035^3,...
%         1.055*sum(inFAT_anatomical(:,i)+admixture_anatomical(:,i),2)*.035^3);
    mdl = fitlm(log(1.055*sum(scar_anatomical(:,i),2)*.035^3+1e-4),...
         log(1.055*sum(inFAT_anatomical(:,i),2)*.035^3+1e-4));
    Xdomain = linspace(0,1.055*0.035^3*...
        max([max(sum(inFAT_anatomical(:,i),2)),...
        max(sum(scar_anatomical(:,i),2))])+0.1,100);
    Xdomain = log(Xdomain+1e-2);
    Ydomain = mdl.Coefficients.Estimate(1) + mdl.Coefficients.Estimate(2)*Xdomain;
    plot(exp(Xdomain),exp(Ydomain),'Color',colors(i,:),'LineStyle','-','linewidth',2)
    plot((1.055*0.035^3*sum(scar_anatomical(:,i),2)+1e-4),...
        (1.055*0.035^3*sum(inFAT_anatomical(:,i),2)+1e-4),...
        'Color',colors(i,:),'LineStyle','none','Marker','.','MarkerSize',20);
    all_pvals(i) = p_val;
    all_rhovals(i) = rho_val;
end
hold off
legend('SeptalFit','Septum','ApexFit','Apex','AnteriorFit',...
    'Anterior','InferiorFit','Inferior')
xlabel('Amount of Scar on LGE-MRI (cm^3)','fontsize',14)
ylabel('Amount of inFAT on CE-CT (cm^3)','fontsize',14)
title('Comparing Arrhythmogenic Substrate per Patient by AHA segments','fontsize',16)
hold off
%****************************%
% Determine correlation for each anatomical segment including the
% heterogeneous mixtures of tissue
% Log transform these variables because they are right skewed
% Rationale: A given infarct is not likely to affect all 4 anatomical
% regions simultaneously. Hence, it is a "discrete" event and will manifest
% as 0 in some regions which will naturally skew the distribution
% accordingly. Thus, we log transform each variable to achieve approximate
% normality and perform correlations accordingly
% We did NOT do this for TOTAL amount of fat or scar because this variable
% is more normally distributed given that ALL patients had infarcts and
% hence would be expected to have non-zero amounts of substrate

colors = linspecer(4);
all_pvals = zeros(4,1);
all_rhovals = all_pvals;
figure();hold on;

for i = 1:4
    [rho_val,p_val] = corr((sum(scar_anatomical(:,i)+greyZone_anatomical(:,i),2)*.035^3),...
        (sum(inFAT_anatomical(:,i)+admixture_anatomical(:,i),2)*.035^3));
    mdl = fitlm(log(1.055*sum(scar_anatomical(:,i)+greyZone_anatomical(:,i),2)*.035^3+1e-4),...
         log(1.055*sum(inFAT_anatomical(:,i)+admixture_anatomical(:,i),2)*.035^3+1e-4));
    Xdomain = linspace(0,1.055*0.035^3*...
        max([max(sum(inFAT_anatomical(:,i)+admixture_anatomical(:,i),2)),...
        max(sum(scar_anatomical(:,i)+greyZone_anatomical(:,i),2))])+0.1,100);
    Xdomain = log(Xdomain+1e-2);
    Ydomain = mdl.Coefficients.Estimate(1) + mdl.Coefficients.Estimate(2)*Xdomain;
    plot(exp(Xdomain),exp(Ydomain),'Color',colors(i,:),'LineStyle','-','linewidth',2)
    plot((1.055*0.035^3*sum(scar_anatomical(:,i)+greyZone_anatomical(:,i),2)+1e-4),...
        (1.055*0.035^3*sum(inFAT_anatomical(:,i)+admixture_anatomical(:,i),2)+1e-4),...
        'Color',colors(i,:),'LineStyle','none','Marker','.','MarkerSize',20);
    all_pvals(i) = p_val;
    all_rhovals(i) = rho_val;
end
hold off
legend('SeptalFit','Septum','ApexFit','Apex','AnteriorFit',...
    'Anterior','InferiorFit','Inferior')
xlabel('Scar + Grey Zone on LGE-MRI (g)','fontsize',14)
ylabel('inFAT + Admixture on CE-CT (g)','fontsize',14)
title('Comparing Arrhythmogenic Substrate per Patient','fontsize',14)
axis tight

%****************************%

colors = linspecer(3);
all_pvals = zeros(3,1);
all_rhovals = all_pvals;
figure();hold on;
for i = 1:3
    [rho_val,p_val] = corr((sum(scar_z(:,i)+greyZone_z(:,i),2)*.035^3+1e-4),...
        (sum(inFAT_z(:,i)+admixture_z(:,i),2)*.035^3+1e-4));
    mdl = fitlm(log(1.055*sum(scar_z(:,i)+greyZone_z(:,i),2)*.035^3+1e-4),...
         log(1.055*sum(inFAT_z(:,i)+admixture_z(:,i),2)*.035^3+1e-4));
    Xdomain = linspace(0,1.055*0.035^3*...
        max([max(sum(inFAT_z(:,i)+admixture_z(:,i),2)),...
        max(sum(scar_z(:,i)+greyZone_z(:,i),2))])+0.1,100);
    Xdomain = log(Xdomain+1e-2);
    Ydomain = mdl.Coefficients.Estimate(1) + mdl.Coefficients.Estimate(2)*Xdomain;
    plot(exp(Xdomain),exp(Ydomain),'Color',colors(i,:),'LineStyle','-','linewidth',2)
    plot((1.055*0.035^3*sum(scar_z(:,i)+greyZone_z(:,i),2)+1e-4),...
        (1.055*0.035^3*sum(inFAT_z(:,i)+admixture_z(:,i),2)+1e-4),...
        'Color',colors(i,:),'LineStyle','none','Marker','.','MarkerSize',20);
    all_pvals(i) = p_val;
    all_rhovals(i) = rho_val;
end
hold off
legend('BaseFit','Base','MidFit','Mid','ApexFit',...
    'Apex')
xlabel('Scar + Grey Zone on LGE-MRI (g)','fontsize',14)
ylabel('inFAT + Admixture on CE-CT (g)','fontsize',14)
title('Comparing Arrhythmogenic Substrate per Patient','fontsize',14)
axis tight

%****************************%

colors = linspecer(7);
all_pvals = zeros(7,1);
all_rhovals = all_pvals;
figure();hold on;
for i = 1:7
    [rho_val,p_val] = corr((sum(scar_bothaz(:,i)+greyZone_bothaz(:,i),2)*.035^3),...
        (sum(inFAT_bothaz(:,i)+admixture_bothaz(:,i),2)*.035^3));
     mdl = fitlm(1.055*sum(scar_bothaz(:,i)+greyZone_bothaz(:,i),2)*.035^3,...
         1.055*sum(inFAT_bothaz(:,i)+admixture_bothaz(:,i),2)*.035^3);
%    mdl = fitlm(log(1.055*sum(scar_bothaz(:,i)+greyZone_bothaz(:,i),2)*.035^3+1e-4),...
%         log(1.055*sum(inFAT_bothaz(:,i)+admixture_bothaz(:,i),2)*.035^3+1e-4));
    Xdomain = linspace(0,1.055*0.035^3*...
        max([max(sum(inFAT_bothaz(:,i)+admixture_bothaz(:,i),2)),...
        max(sum(scar_bothaz(:,i)+greyZone_bothaz(:,i),2))])+0.1);
    %Xdomain = log(Xdomain+1e-2);
    Ydomain = mdl.Coefficients.Estimate(1) + mdl.Coefficients.Estimate(2)*Xdomain;
    plot((Xdomain),(Ydomain),'Color',colors(i,:),'LineStyle','-','linewidth',2)
    plot((1.055*0.035^3*sum(scar_bothaz(:,i)+greyZone_bothaz(:,i),2)),...
        (1.055*0.035^3*sum(inFAT_bothaz(:,i)+admixture_bothaz(:,i),2)),...
        'Color',colors(i,:),'LineStyle','none','Marker','.','MarkerSize',20);
    all_pvals(i) = p_val;
    all_rhovals(i) = rho_val;
end
hold off
legend('BaseSeptumFit','BaseSeptum','BaseAnteriorFit','BaseAnterior',...
    'BaseInferiorFit','BaseInferior','MidSeptumFit','MidSeptum',...
    'MidAnteriorFit','MidAnterior','MidInferiorFit','MidInferior',...
    'ApexFit','Apex')
xlabel('Scar + Grey Zone on LGE-MRI (g)','fontsize',14)
ylabel('inFAT + Admixture on CE-CT (g)','fontsize',14)
title('Comparing Arrhythmogenic Substrate per Patient','fontsize',14)
axis tight

%% Fig 2B bullseye plot for assessing prevalence of scar and inFAT across patients:
 S1 = (scar_bothaz + greyZone_bothaz)./normalMRI_bothaz;
F1 = (inFAT_bothaz+admixture_bothaz)./normalCT_bothaz;
SF = (S1>=.01)==(F1>=.01);
sum(SF,1)/24*100 % This gives all the segments with >=1% volume of segment with remodeling
