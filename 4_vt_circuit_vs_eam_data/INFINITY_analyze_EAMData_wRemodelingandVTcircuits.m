% Written by Eric Sung
% INFINITY_analyze_EAMData_wRemodelingandVTcircuits.m
% Goes through VT circuits (from JHU cohort) and co-registered EAM data to
% compare the two
% This corresponds to the final analysis and Figure 6 of Sung et. al 2022


%% Section 1: looks through ablation data for CT and MRI models 
%Look through the remodeled tissues that were ablated first
% This section assesses correlation between ablated scar and inFAT tissues
fat_ABL = []; admixture_ABL = fat_ABL;
scar_ABL = fat_ABL; gz_ABL = fat_ABL;
scar_onCT_ABL = fat_ABL; gz_onCT_ABL = fat_ABL;
load('ablation_scarandfat_VTcircuit.mat') % Load in co-registered ablated tissue data in scar and fat
faData = fieldnames(ablationData);
for f = 1:length(fieldnames(ablationData))
if ~isempty(regexp(faData{f},'CT'))
    fat_ABL = [fat_ABL;ablationData.(faData{f}).dense_ABL];
    admixture_ABL = [admixture_ABL;ablationData.(faData{f}).intermediate_ABL];
    scar_onCT_ABL = [scar_onCT_ABL;ablationData.(faData{f}).scar_onCT_ABL];
    gz_onCT_ABL = [gz_onCT_ABL;ablationData.(faData{f}).gz_onCT_ABL];
elseif ~isempty(regexp(faData{f},'MRI'))
    scar_ABL = [scar_ABL;ablationData.(faData{f}).dense_ABL];
    gz_ABL = [gz_ABL;ablationData.(faData{f}).intermediate_ABL];
end
end

[rho p ] = corr(fat_ABL+admixture_ABL,scar_ABL+gz_ABL) % Fairly robust correlation suggesting co-localization of ablations toregions of fat and scar
plot(fat_ABL+admixture_ABL,scar_ABL+gz_ABL,'.','MarkerSize',50)

%% Section 2: association of ablations with the actual VT circuits
% Go through the VT circuits and look at the association of ablations between 
% CT model and MRI model VT circuits
fat_ABL_VT = []; admixture_ABL_VT = fat_ABL_VT;
scar_ABL_VT = fat_ABL_VT; gz_ABL_VT = fat_ABL_VT;
scar_onCT_ABL_VT = fat_ABL_VT; gz_onCT_ABL_VT = fat_ABL_VT;
inFAT_onMRI_ABL_VT = fat_ABL_VT; admixture_onMRI_ABL_VT = fat_ABL_VT; 
CT_ABL_VT = []; MRI_ABL_VT = [];

load('INFINITY_scarandfat_VTcircuit.mat') % loads in information about fat and scar tissue in VT circuits

fnames_sfVT = fieldnames(scarandfat_VTcircuit);

all_scar_remodeling = []; % Scar in MRI model VT circuits
all_fat_remodeling = []; % inFAT in CT model VT circuits
all_scar_opposite_remodeling = []; % Looking at scar in CT model VT circuits
all_fat_opposite_remodeling = []; % Looking at inFAT in MRI model VT circuits

% Compute the amount of inFAT and scar tissue in each VT circuit and the
% amount of ablated tissue in each VT circuit component
faData = fieldnames(ablationData);
modelTag = [];
vols = [];
for f = 1:length(fieldnames(ablationData))
    if ~isfield(ablationData.(faData{f}),'dense_in_VTcircuit')
        continue
    end
    % First go through and figure out the amount of fat and scar in all VT
    % circuits
    F = find(~cellfun(@isempty,regexp(fnames_sfVT,faData{f})));
    total_tissue = scarandfat_VTcircuit.(fnames_sfVT{F}).intermediate+...
            scarandfat_VTcircuit.(fnames_sfVT{F}).dense+...
            scarandfat_VTcircuit.(fnames_sfVT{F}).normal;
    total_tissue_opposite = scarandfat_VTcircuit.(fnames_sfVT{F}).intermediate_opposite+...
            scarandfat_VTcircuit.(fnames_sfVT{F}).dense_opposite+...
            scarandfat_VTcircuit.(fnames_sfVT{F}).normal_opposite;
    if ~isempty(regexp(fnames_sfVT{F},'MRI'))
        all_scar_remodeling = [all_scar_remodeling;...
            (scarandfat_VTcircuit.(fnames_sfVT{F}).intermediate+...
            scarandfat_VTcircuit.(fnames_sfVT{F}).dense)./total_tissue.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes];
        all_fat_opposite_remodeling = [all_fat_opposite_remodeling;...
            (scarandfat_VTcircuit.(fnames_sfVT{F}).intermediate_opposite+...
            scarandfat_VTcircuit.(fnames_sfVT{F}).dense_opposite)./total_tissue_opposite.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes_o];
    elseif ~isempty(regexp(fnames_sfVT{F},'CT'))
        all_fat_remodeling = [all_fat_remodeling;...
            (scarandfat_VTcircuit.(fnames_sfVT{F}).intermediate+...
            scarandfat_VTcircuit.(fnames_sfVT{F}).dense)./total_tissue.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes];
        all_scar_opposite_remodeling = [all_scar_opposite_remodeling;...
            (scarandfat_VTcircuit.(fnames_sfVT{F}).intermediate_opposite+...
            scarandfat_VTcircuit.(fnames_sfVT{F}).dense_opposite)./total_tissue_opposite.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes_o];
    end
    
    % Next go through the amount of ablated tissue in the different parts
    % of the VT circuit
    if ~isempty(regexp(faData{f},'CT'))
        CT_ABL_VT = [CT_ABL_VT;ablationData.(faData{f}).normal_in_VTcircuit./total_tissue.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes];
        fat_ABL_VT = [fat_ABL_VT;ablationData.(faData{f}).dense_in_VTcircuit./total_tissue.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes];
        admixture_ABL_VT = [admixture_ABL_VT;ablationData.(faData{f}).intermediate_in_VTcircuit./total_tissue.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes];
        scar_onCT_ABL_VT = [scar_onCT_ABL_VT;ablationData.(faData{f}).scar_onCT_in_VTcircuit./total_tissue_opposite.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes_o];
        gz_onCT_ABL_VT = [gz_onCT_ABL_VT;ablationData.(faData{f}).gz_onCT_in_VTcircuit./total_tissue_opposite.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes_o];
    elseif ~isempty(regexp(faData{f},'MRI'))
        MRI_ABL_VT = [MRI_ABL_VT;ablationData.(faData{f}).normal_in_VTcircuit./total_tissue.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes];
        scar_ABL_VT = [scar_ABL_VT;ablationData.(faData{f}).dense_in_VTcircuit./total_tissue.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes];
        gz_ABL_VT = [gz_ABL_VT;ablationData.(faData{f}).intermediate_in_VTcircuit./total_tissue.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes];
        inFAT_onMRI_ABL_VT = [inFAT_onMRI_ABL_VT;ablationData.(faData{f}).inFAT_onMRI_in_VTcircuit./total_tissue_opposite.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes_o];
        admixture_onMRI_ABL_VT = [admixture_onMRI_ABL_VT;ablationData.(faData{f}).admixture_onMRI_in_VTcircuit./total_tissue_opposite.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes_o];
        modelTag = [modelTag; f/2*ones(length(total_tissue),1)];
        vols = [vols; scarandfat_VTcircuit.(fnames_sfVT{F}).volumes];

    end
end

% Assess correlations of ablated scar and inFAT in the VT circuits
[rho p] = corr(fat_ABL_VT+admixture_ABL_VT,scar_onCT_ABL_VT+gz_onCT_ABL_VT) % Moderate significant correlation between ablated tissues on CT circuits
[rho p] = corr(scar_ABL_VT+gz_ABL_VT,inFAT_onMRI_ABL_VT+admixture_onMRI_ABL_VT)
MM = [scar_ABL_VT; scar_onCT_ABL_VT]+[gz_ABL_VT; gz_onCT_ABL_VT];
CC = [inFAT_onMRI_ABL_VT;fat_ABL_VT]+[admixture_onMRI_ABL_VT;admixture_ABL_VT];
MM_perc = MM./(MM+[MRI_ABL_VT; CT_ABL_VT]); 
CC_perc = CC./(CC+[MRI_ABL_VT; CT_ABL_VT]);
MM_perc(isnan(MM_perc)) = 0;
CC_perc(isnan(CC_perc)) = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Look at scar and fat as covariates in just the MRI only model
% The argument being that LGE-MRI models do not discriminate
% inFAT and hence, it is unusual that inFAT predicts ablation
% correspondence but not the scar.
% Control for: VT circuit size, patient age, and infarct age
X = [all_scar_remodeling all_fat_opposite_remodeling]/1e3*1.055;
missing_VTs = setdiff([1:10],unique(modelTag));
mat_modelTag = dummyvar(categorical(modelTag));
ages = [57 73 81 81 64 53 79 73 84 74]'; % Patient ages
infarct_age = [10.5 22.7 49 22.1 9.7 25.1 5.8 NaN NaN 5.8]'; % Infarct ages
infarct_age(isnan(infarct_age)) = mean(infarct_age(~isnan(infarct_age))); % Mean imputation
ages(missing_VTs) = []; infarct_age(missing_VTs) = []; % Remove for patients without VTs
XX = [X mat_modelTag*ages mat_modelTag*infarct_age vols/1e3];
y = [MRI_ABL_VT] + [scar_ABL_VT] + [gz_ABL_VT]; 
y = y/1e3; % Predict as volume
[B dev stats] = glmfit(XX,y)


%% Section 3: Go through CTMRI models with both scar and fat and look at associations with ablation
%Now look at the CTMRI models with both scar and fat 
load('ablation_scarandfat_VTcircuit_CTMRI.mat')
load('INFINITY_scarandfat_VTcircuit.mat') % loads in scarandfat_VTcircuit
CTMRI_data = load('INFINITY_scarandfat_VTcircuit_CTMRI.mat','scarandfat_VTcircuit');
scarandfat_VTcircuit_CTMRI = CTMRI_data.scarandfat_VTcircuit;

fnames_sfVT = fieldnames(scarandfat_VTcircuit);
fnames_sfVT_CTMRI = fieldnames(scarandfat_VTcircuit_CTMRI);

all_scar_remodeling = [];
all_fat_remodeling = [];
all_scar_opposite_remodeling = [];
all_fat_opposite_remodeling = [];

fat_ABL_VT = []; admixture_ABL_VT = fat_ABL_VT;
scar_ABL_VT = fat_ABL_VT; gz_ABL_VT = fat_ABL_VT;
admixture_gz_ABL_VT = []; fat_scar_ABL_VT = [];

all_scar_remodeling_CTMRI = [];
all_fat_remodeling_CTMRI = [];
all_scarandfat_remodeling_CTMRI = [];

CTMRI_ABL_VT = []; MRI_ABL_VT = [];
faData = fieldnames(ablationData);
vols = [];
modelTag = [];
for f = 1:length(fieldnames(ablationData))
%     if ~isfield(ablationData.(faData{f}),'dense_in_VTcircuit')
%         continue
%     end

    % First look at the structural composition of the VT circuits
    F = strcmp(fnames_sfVT_CTMRI,faData{f});
    total_tissue_CTMRI = scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).normal+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).admix_gz+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).admix+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).gz+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).fat_scar+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).scar+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).fat;
    vols = [vols; scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];

    all_scar_remodeling_CTMRI = [all_scar_remodeling_CTMRI;...
            (scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).gz+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).scar)./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    all_fat_remodeling_CTMRI = [all_fat_remodeling_CTMRI;...
            (scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).admix+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).fat)./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    all_scarandfat_remodeling_CTMRI = [all_scarandfat_remodeling_CTMRI;...
            (scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).admix_gz+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).fat_scar)./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    
    % Next look at the ablated tissue in the VT circuits
    CTMRI_ABL_VT = [CTMRI_ABL_VT;ablationData.(faData{f}).normal_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    fat_ABL_VT = [fat_ABL_VT;ablationData.(faData{f}).fat_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    admixture_ABL_VT = [admixture_ABL_VT;ablationData.(faData{f}).admixture_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    admixture_gz_ABL_VT = [admixture_gz_ABL_VT;ablationData.(faData{f}).admixture_gz_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    fat_scar_ABL_VT = [fat_scar_ABL_VT;ablationData.(faData{f}).fat_scar_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    scar_ABL_VT = [scar_ABL_VT;ablationData.(faData{f}).scar_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    gz_ABL_VT = [gz_ABL_VT;ablationData.(faData{f}).gz_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    modelTag = [modelTag; f*ones(length(total_tissue_CTMRI),1)];

end
[rho p] = corr(fat_ABL_VT+admixture_ABL_VT+fat_scar_ABL_VT+admixture_gz_ABL_VT,...
            scar_ABL_VT+gz_ABL_VT+fat_scar_ABL_VT+admixture_gz_ABL_VT) % Moderate significant correlation between ablated tissues on CT circuits

CC = [fat_ABL_VT+admixture_ABL_VT+fat_scar_ABL_VT+admixture_gz_ABL_VT];
MM= [scar_ABL_VT+gz_ABL_VT+fat_scar_ABL_VT+admixture_gz_ABL_VT];

MM_perc = MM./(MM+[MRI_ABL_VT; CTMRI_ABL_VT]); 
CC_perc = CC./(CC+[MRI_ABL_VT; CTMRI_ABL_VT]);
MM_perc(isnan(MM_perc)) = 0;
CC_perc(isnan(CC_perc)) = 0;

X = 1.055*[all_scar_remodeling_CTMRI+all_scarandfat_remodeling_CTMRI ...
 all_fat_remodeling_CTMRI+all_scarandfat_remodeling_CTMRI]/1e3;
y = [fat_ABL_VT + admixture_ABL_VT + admixture_gz_ABL_VT + ...
    fat_scar_ABL_VT + scar_ABL_VT + gz_ABL_VT]/1e3; 

% Account for age, infarct age, and myocardial volume
mat_modelTag = dummyvar(categorical(modelTag));
ages = [57 73 81 81 64 53 79 73 84 74]'; % Patient age
infarct_age = [10.5 22.7 49 22.1 9.7 25.1 5.8 NaN NaN 5.8]'; % Patient infarct age
infarct_age(isnan(infarct_age)) = mean(infarct_age(~isnan(infarct_age))); % Mean imputation
XX = [X mat_modelTag*ages mat_modelTag*infarct_age vols/1e3];
[B dev stats] = glmfit(XX,y)
stats.p
stats.beta

%% Section 4: look through deceleration zones for CTMRI models
% Go through deceleration zones to see the association with VT circuits and EAM data
% for CTMRI models
load('ablation_scarandfat_VTcircuit_CTMRI.mat') % loads in ablationData
CTMRI_data = load('INFINITY_scarandfat_VTcircuit_CTMRI.mat','scarandfat_VTcircuit');
scarandfat_VTcircuit_CTMRI = CTMRI_data.scarandfat_VTcircuit;

fnames_sfVT_CTMRI = fieldnames(scarandfat_VTcircuit_CTMRI);

all_scar_remodeling = [];
all_fat_remodeling = [];
all_scar_opposite_remodeling = [];
all_fat_opposite_remodeling = [];

fat_DZ_VT = []; admixture_DZ_VT = fat_DZ_VT;
scar_DZ_VT = fat_DZ_VT; gz_DZ_VT = fat_DZ_VT;
admixture_gz_DZ_VT = []; fat_scar_DZ_VT = [];

all_scar_remodeling_CTMRI = [];
all_fat_remodeling_CTMRI = [];
all_scarandfat_remodeling_CTMRI = [];

CTMRI_DZ_VT = [];
faData = fieldnames(ablationData);
modelTag = [];
vols = [];
for f = 1:length(fieldnames(ablationData))
    % Same as before, go through the tissue composition in the VT circuits
    F = strcmp(fnames_sfVT_CTMRI,faData{f});
    total_tissue_CTMRI = scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).normal+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).admix_gz+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).admix+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).gz+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).fat_scar+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).scar+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).fat;
    vols = [vols; scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    all_scar_remodeling_CTMRI = [all_scar_remodeling_CTMRI;...
            (scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).gz+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).scar)./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    all_fat_remodeling_CTMRI = [all_fat_remodeling_CTMRI;...
            (scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).admix+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).fat)./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    all_scarandfat_remodeling_CTMRI = [all_scarandfat_remodeling_CTMRI;...
            (scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).admix_gz+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).fat_scar)./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    
    % Now look at the deceleration zones present in the VT circuits
    CTMRI_DZ_VT = [CTMRI_DZ_VT;ablationData.(faData{f}).normalDZ_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    fat_DZ_VT = [fat_DZ_VT;ablationData.(faData{f}).fatDZ_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    admixture_DZ_VT = [admixture_DZ_VT;ablationData.(faData{f}).admixtureDZ_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    admixture_gz_DZ_VT = [admixture_gz_DZ_VT;ablationData.(faData{f}).admixture_gzDZ_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];%./total_tissue_CTMRI];
    fat_scar_DZ_VT = [fat_scar_DZ_VT;ablationData.(faData{f}).fat_scarDZ_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    scar_DZ_VT = [scar_DZ_VT;ablationData.(faData{f}).scarDZ_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    gz_DZ_VT = [gz_DZ_VT;ablationData.(faData{f}).gzDZ_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    modelTag = [modelTag; f*ones(length(total_tissue_CTMRI),1)];
end

% Perform multivariate regression analysis controlling for tissue volume,
% patient age, infarct age
X = 1.055*[all_scar_remodeling_CTMRI+all_scarandfat_remodeling_CTMRI ...
 all_fat_remodeling_CTMRI+all_scarandfat_remodeling_CTMRI]/1e3; % Should be in units of g
y = [CTMRI_DZ_VT + fat_DZ_VT + admixture_DZ_VT + admixture_gz_DZ_VT ...
    + fat_scar_DZ_VT + scar_DZ_VT + gz_DZ_VT]/1e3; % Predict volume in cm^3
mat_modelTag = dummyvar(categorical(modelTag));
ages = [57 73 81 81 64 53 79 73 84 74]'; % Patient age
infarct_age = [10.5 22.7 49 22.1 9.7 25.1 5.8 NaN NaN 5.8]'; % Infarct age
infarct_age(isnan(infarct_age)) = mean(infarct_age(~isnan(infarct_age))); % Mean imputation
XX = [X mat_modelTag*ages mat_modelTag*infarct_age vols/1e3];
[B dev stats] = glmfit(XX,y);
stats.p
stats.beta

%% Section 5: Deceleration zones for CT and MRI models individually
% Go through deceleration zones to see the association with VT circuits and EAM data
% for CT and MRI models individuall
load('ablation_scarandfat_VTcircuit.mat') % loads in ablationData
load('INFINITY_scarandfat_VTcircuit.mat') % loads in scarandfat_VTcircuit

fnames_sfVT = fieldnames(scarandfat_VTcircuit);

fat_DZ_VT = []; admixture_DZ_VT = fat_DZ_VT;
scar_DZ_VT = fat_DZ_VT; gz_DZ_VT = fat_DZ_VT;
scar_onCT_DZ_VT = fat_DZ_VT; gz_onCT_DZ_VT = fat_DZ_VT;
inFAT_onMRI_DZ_VT = fat_DZ_VT; admixture_onMRI_DZ_VT = fat_DZ_VT; 
CT_DZ_VT = []; MRI_DZ_VT = [];

all_scar_remodeling_CTMRI = [];
all_fat_remodeling_CTMRI = [];
all_scarandfat_remodeling_CTMRI = [];

total_tissue_inVT = [];

all_scar_remodeling = [];
all_fat_remodeling = [];
all_scar_opposite_remodeling = [];
all_fat_opposite_remodeling = [];

faData = fieldnames(ablationData);
modelTag = [];
vols = [];
for f = 1:length(fieldnames(ablationData))
    if ~isfield(ablationData.(faData{f}),'dense_in_VTcircuit')
        continue
    end
    F = find(~cellfun(@isempty,regexp(fnames_sfVT,faData{f})));
    total_tissue = scarandfat_VTcircuit.(fnames_sfVT{F}).intermediate+...
            scarandfat_VTcircuit.(fnames_sfVT{F}).dense+...
            scarandfat_VTcircuit.(fnames_sfVT{F}).normal;
    total_tissue_opposite = scarandfat_VTcircuit.(fnames_sfVT{F}).intermediate_opposite+...
            scarandfat_VTcircuit.(fnames_sfVT{F}).dense_opposite+...
            scarandfat_VTcircuit.(fnames_sfVT{F}).normal_opposite;
    if ~isempty(regexp(fnames_sfVT{F},'MRI'))
        all_scar_remodeling = [all_scar_remodeling;...
            (scarandfat_VTcircuit.(fnames_sfVT{F}).intermediate+...
            scarandfat_VTcircuit.(fnames_sfVT{F}).dense)./total_tissue.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes];
        all_fat_opposite_remodeling = [all_fat_opposite_remodeling;...
            (scarandfat_VTcircuit.(fnames_sfVT{F}).intermediate_opposite+...
            scarandfat_VTcircuit.(fnames_sfVT{F}).dense_opposite)./total_tissue_opposite.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes_o];
    elseif ~isempty(regexp(fnames_sfVT{F},'CT'))
        all_fat_remodeling = [all_fat_remodeling;...
            (scarandfat_VTcircuit.(fnames_sfVT{F}).intermediate+...
            scarandfat_VTcircuit.(fnames_sfVT{F}).dense)./total_tissue.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes];
        all_scar_opposite_remodeling = [all_scar_opposite_remodeling;...
            (scarandfat_VTcircuit.(fnames_sfVT{F}).intermediate_opposite+...
            scarandfat_VTcircuit.(fnames_sfVT{F}).dense_opposite)./total_tissue_opposite.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes_o];
    end
    if ~isempty(regexp(faData{f},'CT'))
        CT_DZ_VT = [CT_DZ_VT;ablationData.(faData{f}).normalDZ_in_VTcircuit./total_tissue.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes];
        fat_DZ_VT = [fat_DZ_VT;ablationData.(faData{f}).denseDZ_in_VTcircuit./total_tissue.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes];
        admixture_DZ_VT = [admixture_DZ_VT;ablationData.(faData{f}).intermediateDZ_in_VTcircuit./total_tissue.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes];
        scar_onCT_DZ_VT = [scar_onCT_DZ_VT;ablationData.(faData{f}).scar_onCT_DZ_in_VTcircuit./total_tissue_opposite.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes_o];
        gz_onCT_DZ_VT = [gz_onCT_DZ_VT;ablationData.(faData{f}).gz_onCT_DZ_in_VTcircuit./total_tissue_opposite.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes_o];
    elseif ~isempty(regexp(faData{f},'MRI'))
        MRI_DZ_VT = [MRI_DZ_VT;ablationData.(faData{f}).normalDZ_in_VTcircuit./total_tissue.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes];
        scar_DZ_VT = [scar_DZ_VT;ablationData.(faData{f}).denseDZ_in_VTcircuit./total_tissue.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes];
        gz_DZ_VT = [gz_DZ_VT;ablationData.(faData{f}).intermediateDZ_in_VTcircuit./total_tissue.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes];
        inFAT_onMRI_DZ_VT = [inFAT_onMRI_DZ_VT;ablationData.(faData{f}).inFAT_onMRI_DZ_in_VTcircuit./total_tissue_opposite.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes_o];
        admixture_onMRI_DZ_VT = [admixture_onMRI_DZ_VT;ablationData.(faData{f}).admixture_onMRI_DZ_in_VTcircuit./total_tissue_opposite.*...
            scarandfat_VTcircuit.(fnames_sfVT{F}).volumes_o];
        modelTag = [modelTag; f/2*ones(length(total_tissue),1)];
        vols = [vols; scarandfat_VTcircuit.(fnames_sfVT{F}).volumes];
    end
end

% Similar picture for individual models, except scar remodeling is slightly
% significant, although the beta value makes no sense
X = 1.055*[all_scar_remodeling all_fat_opposite_remodeling;all_scar_opposite_remodeling ...
        all_fat_remodeling]/1e4;
y = ([MRI_DZ_VT;CT_DZ_VT] + [scar_DZ_VT;fat_DZ_VT] + [gz_DZ_VT;admixture_DZ_VT]); 
[B dev stats] = glmfit(X,y)

% Do this just for the LGE-MRI model
X = [all_scar_remodeling all_fat_opposite_remodeling]/1e3*1.055;
missing_VTs = setdiff([1:10],unique(modelTag));
mat_modelTag = dummyvar(categorical(modelTag));
ages = [57 73 81 81 64 53 79 73 84 74]'; % No pt340
infarct_age = [10.5 22.7 49 22.1 9.7 25.1 5.8 NaN NaN 5.8]'; % No pt340
infarct_age(isnan(infarct_age)) = mean(infarct_age(~isnan(infarct_age))); % Mean imputation
ages(missing_VTs) = []; infarct_age(missing_VTs) = []; % Remove for patients without VTs
XX = [X mat_modelTag*ages mat_modelTag*infarct_age vols/1e3];
y = ([MRI_DZ_VT] + [scar_DZ_VT] + [gz_DZ_VT]);
y = y/1e3; % Predict just volume
[B dev stats] = glmfit(XX,y)

%% Section 6: Overlap between DZs and ablations for CTMRI models
% Assessing the distribution of DZs and ablations within VT circuits

load('ablation_scarandfat_VTcircuit_CTMRI.mat')
load('INFINITY_scarandfat_VTcircuit.mat') % loads in scarandfat_VTcircuit
load('ABLandDZ_withinVTcircuit_CTMRI.mat') % Loads in ABL_DZ_VTcircuitdata
CTMRI_data = load('INFINITY_scarandfat_VTcircuit_CTMRI.mat','scarandfat_VTcircuit');
scarandfat_VTcircuit_CTMRI = CTMRI_data.scarandfat_VTcircuit;

fnames_sfVT = fieldnames(scarandfat_VTcircuit);
fnames_sfVT_CTMRI = fieldnames(scarandfat_VTcircuit_CTMRI);

all_scar_remodeling = [];
all_fat_remodeling = [];
all_scar_opposite_remodeling = [];
all_fat_opposite_remodeling = [];

fat_ABL_VT = []; admixture_ABL_VT = fat_ABL_VT;
scar_ABL_VT = fat_ABL_VT; gz_ABL_VT = fat_ABL_VT;
admixture_gz_ABL_VT = []; fat_scar_ABL_VT = [];

fat_DZ_VT = []; admixture_DZ_VT = fat_DZ_VT;
scar_DZ_VT = fat_DZ_VT; gz_DZ_VT = fat_DZ_VT;
admixture_gz_DZ_VT = []; fat_scar_DZ_VT = [];

CTMRI_DZ_VT = [];
all_scar_remodeling_CTMRI = [];
all_fat_remodeling_CTMRI = [];
all_scarandfat_remodeling_CTMRI = [];

CTMRI_ABL_VT = []; MRI_ABL_VT = [];

faData = fieldnames(ABL_DZ_VTcircuitdata);
vols = [];
vols_conduct = [];
modelTag = [];
perc_breakdown_VTcircuit = [];
all_admixture_CTMRI = [];
all_gz_CTMRI = [];
for f = 1:length(fieldnames(ABL_DZ_VTcircuitdata))
%     if ~isfield(ablationData.(faData{f}),'dense_in_VTcircuit')
%         continue
%     end
    F = strcmp(fnames_sfVT_CTMRI,faData{f});
    total_tissue_CTMRI = scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).normal+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).admix_gz+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).admix+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).gz+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).fat_scar+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).scar+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).fat;
    total_conducting_CTMRI = scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).normal+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).admix_gz+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).admix+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).gz;
    vols = [vols; scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    vols_conduct = [vols_conduct;scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume.*...
                    total_conducting_CTMRI./total_tissue_CTMRI];
                
    % Amounts of tissue in VT circuits
    all_scar_remodeling_CTMRI = [all_scar_remodeling_CTMRI;...
            (scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).gz+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).scar)./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    all_gz_CTMRI = [all_gz_CTMRI;...
            (scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).gz+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).admix_gz)./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    all_fat_remodeling_CTMRI = [all_fat_remodeling_CTMRI;...
            (scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).admix+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).fat)./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    all_admixture_CTMRI = [all_admixture_CTMRI;...
            (scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).admix+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).admix_gz)./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    all_scarandfat_remodeling_CTMRI = [all_scarandfat_remodeling_CTMRI;...
            (scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).admix_gz+...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).fat_scar)./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    
    % Amount of ablated tissue in VT circuit
    CTMRI_ABL_VT = [CTMRI_ABL_VT;ABL_DZ_VTcircuitdata.(faData{f}).normal_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    fat_ABL_VT = [fat_ABL_VT;ABL_DZ_VTcircuitdata.(faData{f}).fat_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    admixture_ABL_VT = [admixture_ABL_VT;ABL_DZ_VTcircuitdata.(faData{f}).admixture_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    admixture_gz_ABL_VT = [admixture_gz_ABL_VT;ABL_DZ_VTcircuitdata.(faData{f}).admixture_gz_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    fat_scar_ABL_VT = [fat_scar_ABL_VT;ABL_DZ_VTcircuitdata.(faData{f}).fat_scar_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    scar_ABL_VT = [scar_ABL_VT;ABL_DZ_VTcircuitdata.(faData{f}).scar_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    gz_ABL_VT = [gz_ABL_VT;ABL_DZ_VTcircuitdata.(faData{f}).gz_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    modelTag = [modelTag; f*ones(length(total_tissue_CTMRI),1)];
    
    % Amount of deceleration zones in VT circuits
    CTMRI_DZ_VT = [CTMRI_DZ_VT;ABL_DZ_VTcircuitdata.(faData{f}).normalDZ_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    fat_DZ_VT = [fat_DZ_VT;ABL_DZ_VTcircuitdata.(faData{f}).fatDZ_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    admixture_DZ_VT = [admixture_DZ_VT;ABL_DZ_VTcircuitdata.(faData{f}).admixtureDZ_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    admixture_gz_DZ_VT = [admixture_gz_DZ_VT;ABL_DZ_VTcircuitdata.(faData{f}).admixture_gzDZ_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];%./total_tissue_CTMRI];
    fat_scar_DZ_VT = [fat_scar_DZ_VT;ABL_DZ_VTcircuitdata.(faData{f}).fat_scarDZ_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    scar_DZ_VT = [scar_DZ_VT;ABL_DZ_VTcircuitdata.(faData{f}).scarDZ_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    gz_DZ_VT = [gz_DZ_VT;ABL_DZ_VTcircuitdata.(faData{f}).gzDZ_in_VTcircuit./total_tissue_CTMRI.*...
            scarandfat_VTcircuit_CTMRI.(fnames_sfVT_CTMRI{F}).volume];
    
    
    perc_breakdown_VTcircuit = [perc_breakdown_VTcircuit;...
        bsxfun(@rdivide,ABL_DZ_VTcircuitdata.(faData{f}).amount_in_VTcircuit,...
        sum(ABL_DZ_VTcircuitdata.(faData{f}).amount_in_VTcircuit,2))];
end

% Convert all tissue amounts to grams
all_admixture_ABL_VT = (admixture_gz_ABL_VT+admixture_ABL_VT)*1.055/1e3;
all_gz_ABL_VT = (gz_ABL_VT + admixture_gz_ABL_VT)*1.055/1e3;
all_admixture_DZ_VT = (admixture_gz_DZ_VT+admixture_DZ_VT)*1.055/1e3;
all_gz_DZ_VT = (gz_DZ_VT + admixture_gz_DZ_VT)*1.055/1e3;

non_overlap_ABL = sum(all_admixture_ABL_VT+all_gz_ABL_VT,2)<=1e-1;
non_overlap_DZ = sum(all_admixture_DZ_VT+all_gz_DZ_VT,2)<=1e-1;

inner_perc = sum(perc_breakdown_VTcircuit(:,5:8),2); % Look at the inner, and outer VT circuit
outer_perc = sum(perc_breakdown_VTcircuit(:,1:4),2);
entrance_perc = sum(perc_breakdown_VTcircuit(:,4),2);
ol_perc = sum(perc_breakdown_VTcircuit(:,2:3),2);
exit_perc = sum(perc_breakdown_VTcircuit(:,1),2);

% Look at the mass of tissue that is conducting
mass_conduct = all_admixture_CTMRI/1000*1.055; % Convert from volume to mass for just the admixture
inner_mass = bsxfun(@times,inner_perc,mass_conduct);
outer_mass = bsxfun(@times,outer_perc,mass_conduct);
all_mass = bsxfun(@times,perc_breakdown_VTcircuit,mass_conduct);

% Look at the amount of tissue at the entrance, exit and outer loop
entrance_mass = bsxfun(@times,entrance_perc,mass_conduct);
exit_mass = bsxfun(@times,exit_perc,mass_conduct);
ol_mass = bsxfun(@times,ol_perc,mass_conduct);

% Look at the amount of deceleration zones in these regions
inner_admixture_DZ_VT = sum(all_admixture_DZ_VT(:,5:8),2);
outer_admixture_DZ_VT = sum(all_admixture_DZ_VT(:,1:4),2);
exit_admixture_DZ_VT = sum(all_admixture_DZ_VT(:,1),2);
entrance_admixture_DZ_VT = sum(all_admixture_DZ_VT(:,4),2);
ol_admixture_DZ_VT = sum(all_admixture_DZ_VT(:,2:3),2);

inner_admix_DZ_perc = inner_admixture_DZ_VT./inner_mass;
outer_admix_DZ_perc = outer_admixture_DZ_VT./outer_mass;
ent_admix_DZ_perc = entrance_admixture_DZ_VT./entrance_mass;
exit_admix_DZ_perc = exit_admixture_DZ_VT./exit_mass;
ol_admix_DZ_perc = ol_admixture_DZ_VT./ol_mass;

y = [inner_admix_DZ_perc ent_admix_DZ_perc ...
    exit_admix_DZ_perc ol_admix_DZ_perc];
[pp tbl, stats] = anova1(y(~non_overlap_DZ,:),...
        {'Common','Entrance','Exit','OuterLoop'})
multcompare(stats)
mean(y(~non_overlap_DZ,:),1) % Percentage of deceleration zones in different parts of the VT circuit

% Do the analysis for ablation lesions as well
% Break down the distribution of ablated tissues in the VT circuit

inner_admixture_ABL_VT = sum(all_admixture_ABL_VT(:,5:8),2);
outer_admixture_ABL_VT = sum(all_admixture_ABL_VT(:,1:4),2);
exit_admixture_ABL_VT = sum(all_admixture_ABL_VT(:,1),2);
entrance_admixture_ABL_VT = sum(all_admixture_ABL_VT(:,4),2);
ol_admixture_ABL_VT = sum(all_admixture_ABL_VT(:,2:3),2);

inner_admix_ABL_perc = inner_admixture_ABL_VT./inner_mass;
outer_admix_ABL_perc = outer_admixture_ABL_VT./outer_mass;
ent_admix_ABL_perc = entrance_admixture_ABL_VT./entrance_mass;
exit_admix_ABL_perc = exit_admixture_ABL_VT./exit_mass;
ol_admix_ABL_perc = ol_admixture_ABL_VT./ol_mass;

y = [inner_admix_ABL_perc ent_admix_ABL_perc ...
    exit_admix_ABL_perc ol_admix_ABL_perc];
[pp tbl, stats] = anova1(y(~non_overlap_ABL,:),...
        {'Common','Entrance','Exit','OuterLoop'})
multcompare(stats)
mean(y(~non_overlap_ABL,:))

